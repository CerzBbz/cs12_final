﻿using System.Windows;

namespace CustomControlLibrary
{
    interface IScrumItemPopup
    {
        bool IsOpen { get; set; }

        void OkButton_Click(object sender, RoutedEventArgs e);
        void CancelButton_Click(object sender, RoutedEventArgs e);

        void OnApplyTemplate();
    }
}
