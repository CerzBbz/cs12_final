﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using ScrumManagementServer.DataContracts.DTO_classes;

namespace CustomControlLibrary
{
    public class TaskPopup : Control, IScrumItemPopup
    {
        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(TaskPopup),
                new PropertyMetadata(false, OnOpenChanged));

        #region defaults

        private const string PopupTitleLabelTextAdd = "Add Task";
        private const string PopupTitleLabelTextEdit = "Edit Task";

        #endregion

        #region Variables

        private TaskDto _initialSource;

        #endregion

        #region UI Elements

        private Popup _dialog;
        private Label _popupTitleLabel;
        private TextBox _titleTextBox, _estimatedHoursTextBox, _remainingHoursTextBox, _completedHoursTextBox, _reasonTextBox;
        private ComboBox _assignedToComboBox, _generalPriorityComboBox, _impactedAreaComboBox, _statusComboBox;
        private Button _okButton, _cancelButton;

        #endregion

        #region Properties

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        public bool CanModifyAssignment { get; set; }

        public TaskDto InitialSource
        {
            get { return _initialSource; }
            set
            {
                if (_initialSource == value) return;
                _initialSource = value;
                OnInitialSourceChanged(EventArgs.Empty);
            }
        }

        public List<UserDto> AssignedToDataSource { get; set; }
        public UserStoryDto CurrentUserStory { get; set; }
        public TaskDto GeneratedTask { get; private set; }

        private IEnumerable<string> DefaultGeneralPriorities { get; set; }

        private IEnumerable<string> DefaultImpactedAreas { get; set; } 

        //ToDo: Push into a table, similar to user roles?
        private IEnumerable<string> DefaultStatuses { get; set; }

        private Regex DigitOnlyRegex{ get; set; }

        #endregion

        #region Events

        public EventHandler Ok;
        public EventHandler Cancel;
        public EventHandler InitialSourceChanged;

        #endregion

        static TaskPopup()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TaskPopup),
                new FrameworkPropertyMetadata(typeof(TaskPopup)));
        }

        public TaskPopup()
        {
            //Initialise the default within the constructor as VS2013 doesn't use C# 6.0
            DefaultGeneralPriorities = new List<string>
            {
                "Critical", "High", "Medium", "Low"
            };

            DefaultImpactedAreas = new List<string>
            {
                "User Interface", "Integration", "Database", "Performance improvements", "Environment modifications"
            };

            DefaultStatuses = new List<string>
            {
                "Not started", "In Progress", "In testing", "Complete", "Blocked"
            };

            DigitOnlyRegex = new Regex("[^0-9]+");
        }

        #region Public methods
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _dialog = GetTemplateChild("dialog") as Popup;
            _popupTitleLabel = GetTemplateChild("popupTitleLabel") as Label;
            _okButton = GetTemplateChild("okButton") as Button;
            _cancelButton = GetTemplateChild("cancelButton") as Button;
            _titleTextBox = GetTemplateChild("titleTextBox") as TextBox;
            _assignedToComboBox = GetTemplateChild("assignedToComboBox") as ComboBox;
            _generalPriorityComboBox = GetTemplateChild("generalPriorityComboBox") as ComboBox;
            _impactedAreaComboBox = GetTemplateChild("impactedAreaComboBox") as ComboBox;
            _statusComboBox = GetTemplateChild("statusComboBox") as ComboBox;
            _estimatedHoursTextBox = GetTemplateChild("estimatedHoursTextBox") as TextBox;
            _remainingHoursTextBox = GetTemplateChild("remainingHoursTextBox") as TextBox;
            _completedHoursTextBox = GetTemplateChild("completedHoursTextBox") as TextBox;
            _reasonTextBox = GetTemplateChild("reasonTextBox") as TextBox;

            SetupInitialDisplays();
        }

        public void OkButton_Click(object sender, RoutedEventArgs e)
        {
            OnOk(EventArgs.Empty);
        }

        public void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            OnCancel(EventArgs.Empty);
        }

        public void NumberTextField_Handled(object sender, TextCompositionEventArgs e)
        {
            //Restricts a TextField to digits only using a regex
            e.Handled = DigitOnlyRegex.IsMatch(e.Text);
        }

        public void Show()
        {
            _dialog.IsOpen = true;
            _dialog.Visibility = Visibility.Visible;
        }

        public void Close()
        {
            Clear();
            _dialog.IsOpen = false;
            _dialog.Visibility = Visibility.Hidden;
        }

        public void Clear()
        {
            _popupTitleLabel.Content = PopupTitleLabelTextAdd;
            _titleTextBox.Text = "";
            _assignedToComboBox.SelectedIndex = -1;
            _generalPriorityComboBox.SelectedIndex = -1;
            _impactedAreaComboBox.SelectedIndex = -1;
            _statusComboBox.SelectedIndex = 0;
            _reasonTextBox.Text = string.Empty;
            _estimatedHoursTextBox.Text = "0";
            _completedHoursTextBox.Text = "0";
            _remainingHoursTextBox.Text = "0";
            _initialSource = null;
        }

        #endregion

        #region Protected Methods
        protected virtual void OnOk(EventArgs e)
        {           
            if (!ManualValidation()) return;

            GeneratedTask = MapToTask();
            if (Ok != null)
                Ok(this, e);
            Close();
        }

        protected virtual void OnCancel(EventArgs e)
        {
            if (Cancel != null)
                Cancel(this, e);
            Close();
        }

        protected virtual void OnInitialSourceChanged(EventArgs e)
        {
            if (InitialSourceChanged != null)
                InitialSourceChanged(this, e);
                SetupFromTask();
        }
        #endregion

        #region Private Methods
        private static void OnOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var taskPopup = (TaskPopup)d;
            var isOpen = (bool)e.NewValue;

            if (isOpen)
            {
                taskPopup.Show();
            }
            else
            {
                taskPopup.Close();
            }

            taskPopup.IsOpen = isOpen;
        }

        private void SetupInitialDisplays()
        {
            if (_popupTitleLabel != null)
                _popupTitleLabel.Content = PopupTitleLabelTextAdd;

            SetupDialog();
            SetupButtons();
            SetupAssignedTo();
            SetupPriorities();
            SetupStatuses();
            SetupImpactedAreas();
            SetupHoursDisplays();
        }

        private void SetupDialog()
        {
            if (_dialog == null) return;

            //Inherit the Width/Height/Margin that were applied to the control, then center within the parent
            _dialog.Width = Width;
            _dialog.Height = Height;
            _dialog.Margin = Margin;
            _dialog.Placement = PlacementMode.Center;
        }

        private void SetupButtons()
        {
            if (_okButton != null)
                _okButton.Click += OkButton_Click;
            if (_cancelButton != null)
                _cancelButton.Click += CancelButton_Click;
        }

        private void SetupAssignedTo()
        {
            if (_assignedToComboBox == null) return;

            if (AssignedToDataSource == null)
                AssignedToDataSource = new List<UserDto>();

            _assignedToComboBox.ItemsSource = AssignedToDataSource;
            _assignedToComboBox.DisplayMemberPath = "Username";
            _assignedToComboBox.IsTextSearchEnabled = CanModifyAssignment;
            _assignedToComboBox.DropDownOpened += assignedToComboBox_DropDownOpened;
        }

        private void SetupHoursDisplays()
        {
            if (_estimatedHoursTextBox != null)
            {
                _estimatedHoursTextBox.Text = "0";
                _estimatedHoursTextBox.MaxLength = 3;
                _estimatedHoursTextBox.PreviewTextInput += NumberTextField_Handled;
            }
            if (_remainingHoursTextBox != null)
            {
                _remainingHoursTextBox.Text = "0";
                _remainingHoursTextBox.MaxLength = 3;
                _remainingHoursTextBox.PreviewTextInput += NumberTextField_Handled;
            }
            if (_completedHoursTextBox != null)
            {
                _completedHoursTextBox.Text = "0";
                _completedHoursTextBox.MaxLength = 3;
                _completedHoursTextBox.PreviewTextInput += NumberTextField_Handled;
            }
        }

        private void SetupPriorities()
        {
            if (_generalPriorityComboBox == null) return;
            _generalPriorityComboBox.ItemsSource = DefaultGeneralPriorities;
        }

        private void SetupImpactedAreas()
        {
            if (_impactedAreaComboBox == null) return;
            _impactedAreaComboBox.ItemsSource = DefaultImpactedAreas;
        }

        private void SetupStatuses()
        {
            if (_statusComboBox == null) return;
            _statusComboBox.ItemsSource = DefaultStatuses;
            _statusComboBox.SelectedIndex = 0;
        }

        private void SetupFromTask()
        {
            //ToDo: Possibly add additional checks before setting values
            _popupTitleLabel.Content = PopupTitleLabelTextEdit;
            _titleTextBox.Text = _initialSource.Title;
            _assignedToComboBox.SelectedItem = AssignedToDataSource.FirstOrDefault(u => u.Id == _initialSource.AssignedToId); //Assume developer still exists
            _generalPriorityComboBox.SelectedItem = _initialSource.GeneralPriority;
            _impactedAreaComboBox.SelectedItem = _initialSource.ImpactedArea;
            _statusComboBox.SelectedItem = _initialSource.Status;
            _reasonTextBox.Text = _initialSource.StatusReason;
            _estimatedHoursTextBox.Text = _initialSource.EstimatedHours.ToString();
            _completedHoursTextBox.Text = _initialSource.CompletedHours.ToString();
            _remainingHoursTextBox.Text = _initialSource.RemainingHours.ToString();
        }

        private TaskDto MapToTask()
        {
            return new TaskDto
            {
                Title = _titleTextBox.Text,
                AssignedToId = (_assignedToComboBox.SelectedItem as UserDto).Id,
                AssociatedUserStoryId = _initialSource == null ? CurrentUserStory.Id : _initialSource.AssociatedUserStoryId,
                EstimatedHours = Convert.ToInt32(_estimatedHoursTextBox.Text),
                CompletedHours = Convert.ToInt32(_completedHoursTextBox.Text),
                RemainingHours = Convert.ToInt32(_remainingHoursTextBox.Text),
                GeneralPriority = _generalPriorityComboBox.SelectedItem as string,
                ImpactedArea = _impactedAreaComboBox.SelectedItem as string,
                Status = _statusComboBox.SelectedItem as string,
                StatusReason = _reasonTextBox.Text
            };
        }

        private bool ManualValidation()
        {
            //Directive to close popup when debugging, uncomment when stepping through validation
#if DEBUG
            //IsOpen = false;
#endif

            var isValid = true;
            var errors = new StringBuilder("Please correct the following errors: \n");

            if (_titleTextBox.Text.Equals(string.Empty))
            {
                errors.AppendLine("\tPlease provide a title for the task");
            }

            var chosenAssignedTo = _assignedToComboBox.SelectedItem as UserDto;
            if (_assignedToComboBox.SelectedItem == null
                || _assignedToComboBox.SelectedValue.ToString().Equals(string.Empty)
                || !AssignedToDataSource.Any(u => u.Equals(chosenAssignedTo)))
            {
                errors.AppendLine("\tPlease provide a valid user to assign the task to");
                isValid = false;
            }
            if (_statusComboBox.SelectedValue.Equals("Blocked") && 
                (_reasonTextBox.Text == null || _reasonTextBox.Text.Trim().Length == 0))
            {
                errors.AppendLine("\tPlease provide a blocked reason");
                isValid = false;
            }

            //ToDo: Custom control for error messages?
            if (!isValid)
            {
                /*
                    Temporarily modify the StaysOpen property to prevent focus being lost and hiding the popup.
                    Specify that the message box should be the default top most element, allowing it to steal this
                    from the popup which has top most priority when StaysOpen is true, even outside an application
                */
                _dialog.StaysOpen = true;
                MessageBox.Show(errors.ToString(), "Errors", MessageBoxButton.OK, 
                    MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                _dialog.StaysOpen = false;
            }

            //Directive to reopen popup when debugging, uncomment when stepping through validation
#if DEBUG
            //IsOpen = true;
#endif

            return isValid;
        }

        private void assignedToComboBox_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            comboBox.IsDropDownOpen = CanModifyAssignment;
            /*
                    Temporarily modify the StaysOpen property to prevent focus being lost and hiding the popup.
                    Specify that the message box should be the default top most element, allowing it to steal this
                    from the popup which has top most priority when StaysOpen is true, even outside an application
                */
            _dialog.StaysOpen = true;
            MessageBox.Show("Only developers are able to modify task assignment", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
            _dialog.StaysOpen = false;
        }

        #endregion
    }
}
