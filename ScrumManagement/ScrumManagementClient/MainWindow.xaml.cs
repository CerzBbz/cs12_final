﻿using System.Windows;
using ScrumManagementClient.LoginService;
using ScrumManagementServer.DataContracts;
using System.Windows.Controls;
using System;
using ScrumManagementClient.Views;

namespace ScrumManagementClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private ILoginService loginService = new LoginServiceClient();
        public MainWindow()
        {
            InitializeComponent();
            Switcher.PageSwitcher = this;
            Switcher.Switch(new LoginPage());
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }


        public void Navigate(UserControl newPage)
        {
            this.Content = newPage;
        }

        public void Navigate(UserControl newPage, object state)
        {
            this.Content = newPage;
            ISwitchable s = newPage as ISwitchable;

            if (s != null)
                s.UtilizeState(state);
            else
                throw new ArgumentException("NextPage is not ISwitchable! "
                                            + newPage.Name.ToString());
        }

    }
}




// private void createUserBtn_Click(object sender, RoutedEventArgs e)
       // {
            //var result = loginService.registerNewUser(Username.Text, Password.Password);
            //if (result.Equals(LoginReturnCode.SUCCESSFUL_REGISTRATION))
            //    MessageBox.Show("Registration successful");
            //else if (result.Equals(LoginReturnCode.INVALID_EMAIL_ADDRESS))
            //{
            //    IncorrectUsernameLbl.Visibility = System.Windows.Visibility.Visible;
            //    MessageBox.Show("Registration unsuccessful - invalid email address provided");
            //}

            //else if (result.Equals(LoginReturnCode.USERNAME_ALREADY_EXISTS))
            //{
            //    IncorrectUsernameLbl.Visibility = System.Windows.Visibility.Visible;
            //    MessageBox.Show("Registration unsuccessful - username already exists");
            //}
            //else if (result.Equals(LoginReturnCode.INVALID_PASSWORD_COMPLEXITY))
            //    MessageBox.Show("Registration unsuccessful - password doesn't meet required complexity");
   // }

        //private void loginBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    var result = loginService.authenticateUserDetails(Username.Text, Password.Password);
        //    if (result.Equals(LoginReturnCode.SUCCESSFUL_LOGIN))
        //        MessageBox.Show("Login successful");
        //    else
        //        IncorrectUsernameLbl.Visibility = System.Windows.Visibility.Visible;
        //        MessageBox.Show("Login unsuccessful");
        //}
    //}

