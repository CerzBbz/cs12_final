﻿using ScrumManagementClient.SprintManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumManagementClient.Navigation
{
    public static class CurrentSprint
    {

        private static readonly ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();
        private static SprintDto _sprint;


        public static SprintDto getSprint()
        {
            return _sprint;
        }

        public static void setSprint(int Id)
        {
            // This will use the ProjectManagement service to return the UserDTO 
            //which has the same username as the logged in user.

            _sprint = _sprintManagementService.GetAllSprints().FirstOrDefault(s => s.Id.Equals(Id));
        }

    }
}
