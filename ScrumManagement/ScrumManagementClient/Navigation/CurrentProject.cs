﻿using ScrumManagementClient.ProjectManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumManagementClient.Navigation
{
    static class CurrentProject
    {

        private static readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private static ProjectDto project;


        public static  ProjectDto getProject()
        {
            return project;
        }

        public static void setProject(string title)
        {
            // This will use the ProjectManagement service to return the UserDTO 
            //which has the same username as the logged in user.

            project = _projectManagementService.GetAllProjects().FirstOrDefault(p => p.Title.Equals(title));
        }

    }
}
