﻿using ScrumManagementClient.ProjectManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumManagementClient.Navigation
{
    public static class UserDetails
    {

        private static readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private static UserDto user;


        public static UserDto getUser()
        {
            return user;
        }

        public static void setUser(string username)
        {
            // This will use the ProjectManagement service to return the UserDTO 
            //which has the same username as the logged in user.

            //Not using lazy loading, it isnt passing the user roles when converting to DTO
            user = _projectManagementService.GetAllUsers().FirstOrDefault(a => a.Username.Equals(username));
        }

    }
}
