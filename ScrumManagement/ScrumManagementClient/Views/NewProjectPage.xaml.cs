﻿using System.Windows;
using System.Windows.Controls;
using ScrumManagementClient.ProjectManagementService;
using IProjectManagementService = ScrumManagementClient.ProjectManagementService.IProjectManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using ScrumManagementClient.Navigation;
using System.Text;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for NewProjectPage.xaml
    /// </summary>
    public partial class NewProjectPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        UserDto user = UserDetails.getUser();

        public NewProjectPage()
        {
            InitializeComponent();
            SetupUserRoles();
            ProjectStartDatePicker.BlackoutDates.Add(new CalendarDateRange(DateTime.MinValue, DateTime.Now.AddDays(-1)));

        }

        private void SetupUserRoles()
        {
            foreach (var item in _projectManagementService.GetUsersWithRole("Product Owner"))
            {
                ProductOwnerCbx.Items.Add(item);
            }
       
        }

        private void CreateProject_Click(object sender, RoutedEventArgs e)
        {            
            if (!ManualValidation()) return;

            var chosenProductOwner = ProductOwnerCbx.SelectedItem as UserDto;
            var projectStartDate = (DateTime)ProjectStartDatePicker.SelectedDate; //Validation ensures this can't be null

            _projectManagementService.CreateProject(ProjectNameTbx.Text, 
                chosenProductOwner != null ? chosenProductOwner.Id : 0,
                user.Id, 
                projectStartDate, ProjectDescTbx.Text);
            Switcher.Switch(new OverviewPage());
        }

        private bool ManualValidation()
        {
            var isValid = true;
            var errors = new StringBuilder("Please correct the following errors: \n");

            var chosenProductOwner = ProductOwnerCbx.SelectedItem as UserDto;
            

            if (ProjectNameTbx.Text.Trim().Equals(string.Empty))
            {
                errors.AppendLine("\tPlease provide a valid name for the project");
                isValid = false;
            }

            //If there is text available, make sure it matches an existing user that was added to the items source
            if (!ProductOwnerCbx.Text.Equals(string.Empty) && chosenProductOwner == null)
            {
                errors.AppendLine("\tPlease provide a valid product owner for the project");
                isValid = false;
            }

            //If there is text available, make sure it matches an existing user that was added to the items source
          

            if (!ProjectStartDatePicker.SelectedDate.HasValue)
            {
                errors.AppendLine("\tPlease provide a start date for the project");
                isValid = false;
            }

            if (!isValid)
                MessageBox.Show(errors.ToString(), "Errors", MessageBoxButton.OK,
                    MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);

            return isValid;
        }      
    }
}
