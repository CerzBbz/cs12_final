﻿using ScrumManagementClient.Navigation;
using ScrumManagementClient.ProjectManagementService;
using ScrumManagementClient.ReferenceDataService;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for UserDetailsPage.xaml
    /// </summary>
    public partial class UserDetailsPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private readonly IReferenceDataService _referenceDataService = new ReferenceDataServiceClient();

        public UserDetailsPage()
        {
            InitializeComponent();

            UserDto user = UserDetails.getUser();

            currentUser.Content = user.Username;
            currentUserSkills.Content = user.Skills;
            currentUser.Content = UserDetails.getUser().Username;
            foreach (var userRole in user.UserRoles)
            {
                //Dispaly each user role on a new line in the text box
                currentUserRoles.Content += userRole.RoleDescription + string.Format("\n");
            }

        }

        private void EditProfileBtn_Click(object sender, RoutedEventArgs e)
        {
            //We will hide some buttons if we are in "Edit Mode"
            SkillsetUpdate.Visibility = Visibility.Visible;
            SkillsetUpdate.Text = currentUserSkills.Content.ToString();
            currentUserSkills.Visibility = Visibility.Hidden;
            SaveChangesBtn.Visibility = Visibility.Visible;
            EditProfileBtn.Visibility = Visibility.Hidden;
            currentUserRoles.Visibility = Visibility.Hidden;
            UserRolesPanel.Visibility = Visibility.Visible;

            UserDto user = UserDetails.getUser();

            //Dynamically create check boxes for all available user roles
            foreach (var role in _referenceDataService.GetAvailableUserRoles())
            {
                //Create checkboxes of all available roles
                //reflect active roles in the UI
                var roleBox = new CheckBox
                {
                    Content = role.RoleDescription,
                    DataContext = role,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = 100,
                    IsChecked = user.UserRoles.Select(a=>a.Id).ToList().Contains(role.Id) ? true : false
                };
                UserRolesPanel.Children.Add(roleBox);
            }

        }

        private void SaveChangesBtn_Click(object sender, RoutedEventArgs e)
        {
            //The projectService needs a User, create one from the current UserDto
            UserDto userDto = UserDetails.getUser();
            User user = new User()
            {
                AssignedTasks = userDto.AssignedTasks as ICollection<ScrumManagementServer.Domain.Task>,
                Id = userDto.Id,
                Password = userDto.Password,
                Salt = userDto.Salt,
                Skills = SkillsetUpdate.Text,
                Username = userDto.Username,
            };

            //Get the selected roles from all checkboxes in UserRolesPanel
            var selectedRoles = this.UserRolesPanel.Children.OfType<CheckBox>()
                .Where(a => a.IsChecked.Value)
                .Select(a => a.DataContext);

            //Only save these to the User if the roles have changed
            if (!selectedRoles.Equals(UserDetails.getUser().UserRoles))
            {
                foreach(var role in selectedRoles)
                {
                    user.UserRoles.Add(role as UserRole);
                }
            }          

            //Update the UserDetails, refresh the Static Class, and return to main view.
            _projectManagementService.UpdateUserdetails(user);
            UserDetails.setUser(userDto.Username);
            RevertEditVisibility();
        }

        private void RevertEditVisibility()
        {
            //Revert the visibility changes that were made when the edit button pressed
            SkillsetUpdate.Visibility = Visibility.Hidden;
            currentUserSkills.Visibility = Visibility.Visible;
            SaveChangesBtn.Visibility = Visibility.Hidden;
            EditProfileBtn.Visibility = Visibility.Visible;

            //The checkboxes for the Users roles will be replaced
            //with the updated text.
            currentUserRoles.Visibility = Visibility.Visible;
            UserRolesPanel.Visibility = Visibility.Hidden;

            //Reset the current user to record the changes          
            UserDto user = UserDetails.getUser();

            //And refresh any of the fields that have changed
            currentUserSkills.Content = user.Skills;

            //Clear and recreate UserRoles
            currentUserRoles.Content = "";
            foreach (var userRole in user.UserRoles)
            {
                //Display each user role on a new line in the text box
                currentUserRoles.Content += userRole.RoleDescription + string.Format("\n");
            }
        }

    }
}
