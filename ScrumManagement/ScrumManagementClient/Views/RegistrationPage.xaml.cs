﻿using ScrumManagementClient.LoginService;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Encryption;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ScrumManagementClient.ReferenceDataService;
using ScrumManagementServer.Domain;
using ILoginService = ScrumManagementClient.LoginService.ILoginService;
using IReferenceDataService = ScrumManagementClient.ReferenceDataService.IReferenceDataService;
using System.Security;
using System;
using System.Runtime.InteropServices;


namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for RegistrationPage.xaml
    /// </summary>
    public partial class RegistrationPage : UserControl
    {
        private readonly ILoginService _loginService = new LoginServiceClient();
        private readonly IReferenceDataService _referenceDataService = new ReferenceDataServiceClient();

        public RegistrationPage()
        {
            InitializeComponent();

            //Dynamically create check boxes for all available user roles
            foreach (var role in _referenceDataService.GetAvailableUserRoles())
            {
                var roleBox = new CheckBox
                {
                    Content = role.RoleDescription,
                    DataContext = role,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = 100
                };
                UserRolesPanel.Children.Add(roleBox);
            }
        }

        private List<UserRole> GetChosenRoles()
        {
            return (from CheckBox cb in UserRolesPanel.Children
                    where cb.IsChecked != null && (cb.IsChecked.Value)
                    select cb.DataContext as UserRole).ToList();
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            var salt = Encryption.CreateSalt(32);

            //Check that the two passwords are matching
            if (ConvertToString(PasswordInput.SecurePassword) != ConvertToString(PasswordInput2.SecurePassword))
            {
                MessageBox.Show("The two passwords do not match", "Error");
                return;
            }

            //use SecureString to pin the password, encrypt it when its not being accessed
            var ssPassword = PasswordInput.SecurePassword;
            //make password readonly
            ssPassword.MakeReadOnly();

            //get rid of password input
            PasswordInput.Clear();
            PasswordInput2.Clear();
            

            var hashSaltPass = ssPassword.Length < 8 ? "" : 
                                        Encryption.GenerateSha256Hash(ssPassword, salt);
            //dispose of password
            ssPassword.Dispose();
            
            var result = _loginService.RegisterNewUser(EmailInput.Text, hashSaltPass, salt, SkillsTxt.Text, GetChosenRoles().ToArray());
            if (result.Equals(LoginReturnCode.SuccessfulRegistration))
            {
                MessageBox.Show("Registration successful");
                Switcher.Switch(new LoginPage());
            }
            else if (result.Equals(LoginReturnCode.InvalidEmailAddress))
            {
                MessageBox.Show("Registration unsuccessful - invalid email address provided");
            }
            else if (result.Equals(LoginReturnCode.UsernameAlreadyExists))
            {
               MessageBox.Show("Registration unsuccessful - username already exists");
            }
            else if (result.Equals(LoginReturnCode.InvalidPasswordComplexity))
            {
                MessageBox.Show("Registration unsuccessful - password doesn't meet required complexity");
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }

        // ref http://blogs.msdn.com/b/fpintos/archive/2009/06/12/how-to-properly-convert-securestring-to-string.aspx
        private string ConvertToString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException("securePassword");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

    }
}
