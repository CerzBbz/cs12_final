﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using ScrumManagementClient.ProjectManagementService;
using IProjectManagementService = ScrumManagementClient.ProjectManagementService.IProjectManagementService;
using ScrumManagementClient.Properties;
using ScrumManagementClient.SprintManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;

namespace ScrumManagementClient.Views
{
    public partial class SprintBacklogManagerPage : UserControl
    {
        private ProjectDto selectedProject;
        private SprintDto selectedSprint;
        private IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();

        public SprintBacklogManagerPage(ProjectDto selectedProject, SprintDto selectedSprint)
        {
            InitializeComponent();
            this.selectedProject = selectedProject;
            this.selectedSprint = selectedSprint;
            PageTitleLbl.Content = (PageTitleLbl.Content + " \"" + selectedSprint.Title + "\" within the project \"" + selectedProject.Title + "\"");
            PopulateLists();
        }

        private void AddStoryBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = ProductBacklogList.SelectedIndex;
            if (selectedIndex >= 0)
            {
                var userStory = ProductBacklogList.SelectedItem as UserStoryDto;
                if (_sprintManagementService.AddUserStoryToSprint(selectedSprint, userStory) == 1)
                {
                    _projectManagementService.SetUserStoryEditableTo(userStory, false);
                    UpdateSprintList();
                }
                else
                {
                    MessageBox.Show("This user story is already attached to this sprint");
                }
            }
            else
            {
                MessageBox.Show("Please select a user story from the Product Backlog");
            }
        }

        private void RemoveStoryBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = SprintBacklogList.SelectedIndex;
            if (selectedIndex >= 0)
            {
                var userStory = SprintBacklogList.SelectedItem as UserStoryDto;
                if (_sprintManagementService.RemoveUserStoryFromSprint(selectedSprint, userStory) == 1)
                { 
                    _projectManagementService.SetUserStoryEditableTo(userStory, true);
                    UpdateSprintList();
                }
                else
                {
                    MessageBox.Show("There was an error removing this sprint from the Sprint Backlog");
                }
            }
            else
            {
                MessageBox.Show("Please select an item from the Sprint Backlog to remove");
            }
        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SprintBacklogPage(selectedProject, selectedSprint));
        }

        private void PopulateLists()
        {
            //Populate the ProductBacklogList from the selectedProject
            var projectStoryList = _projectManagementService.GetAllUserStoriesForProject(selectedProject).ToList();
            ProductBacklogList.ItemsSource = projectStoryList;

            //Populate the SprintBacklogList from the selectedSprint
            UpdateSprintList();
        }

        private void UpdateSprintList()
        {
            var sprintStoryList = _sprintManagementService.GetUserStoriesFromSprintId(selectedSprint.Id).ToList();
            SprintBacklogList.ItemsSource = sprintStoryList;
        }

        private void ViewTasks_Click(object sender, RoutedEventArgs e)
        {
            var userStoryToViewTasksOn = (sender as Button).DataContext as UserStoryDto;
            Switcher.Switch(new ViewUserStoryTasks(userStoryToViewTasksOn));
        }
    }
}
