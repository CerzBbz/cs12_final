﻿using ScrumManagementClient.ProjectManagementService;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for ViewProjectTeamPage.xaml
    /// </summary>
    public partial class ViewProjectTeamPage : UserControl
    {
        IProjectManagementService pms = new ProjectManagementServiceClient();
        /*
        Initialises a datatable, with username column, searches database for projectMembers, if there are any, populate the datatable
        */
        public ViewProjectTeamPage()
        {
            InitializeComponent();
            var projectMembers = pms.GetAllUsersForProjectTemp();
            
            var dt = new DataTable();
            
            dt.Columns.Add("Username");
            dt.Columns.Add("User Role");
            if (projectMembers.Any())
            {
                foreach (var user in projectMembers)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["Username"] = user.Username;
                    int userRoleCount = user.UserRoles.Count;
                    int userRoleAdded = 0;
                    //Will populate the User role row with each user role
                    foreach (var role in user.UserRoles)
                    {
                        newRow["User role"] += role.RoleDescription;
                        
                        //This will add a new line after each user role
                        userRoleAdded += 1;
                        if (userRoleAdded < userRoleCount)
                        {
                            newRow["User role"] += string.Format("\n");
                        }
                    }
                    dt.Rows.Add(newRow);
                    
                }
                teamOverviewGrid.ItemsSource = dt.DefaultView;
            }
            else
            {;
                teamOverviewGrid.ItemsSource = dt.DefaultView;
            }

        }

        private void searchResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        #region ButtonHandlers       
        private void addTeamMember_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new TeamBuilderPage());
        }
        #endregion

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }

        private void homeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

        
    }
}
