﻿using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScrumManagementClient.Navigation;
using ScrumManagementClient.ProjectManagementService;
using ScrumManagementClient.SprintManagementService;
using IProjectManagementService = ScrumManagementClient.ProjectManagementService.IProjectManagementService;
using ISprintManagementService = ScrumManagementClient.SprintManagementService.ISprintManagementService;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for ViewUserStoryTasks.xaml
    /// </summary>
    public partial class ViewUserStoryTasks : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private readonly ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();
        private UserStoryDto _currentUserStory;

        public ViewUserStoryTasks(UserStoryDto userStory)
        {
            InitializeComponent();
            _currentUserStory = userStory;
            userStoryTasksGrid.ItemsSource = userStory.AssociatedTasks;

            var developers = _projectManagementService.GetUsersWithRole("Developer");
            TaskPopup.AssignedToDataSource = developers.ToList();
            TaskPopup.CurrentUserStory = _currentUserStory;
            TaskPopup.CanModifyAssignment = UserDetails.getUser().UserRoles.Any(ur => ur.RoleDescription.Equals("Developer"));
        }

        public void AddNewTask_Click(object sender, RoutedEventArgs e)
        {
            TaskPopup.Ok -= new EventHandler(UpdateTask);
            TaskPopup.Ok += new EventHandler(CreateTask);
            TaskPopup.IsOpen = !TaskPopup.IsOpen;
        }

        private void CreateTask(object sender, EventArgs e)
        {
            var createdTask = TaskPopup.GeneratedTask;
            if (createdTask == null) return;
            _currentUserStory.AssociatedTasks.Add(createdTask);
            _sprintManagementService.UpdateUserStory(_currentUserStory);
            userStoryTasksGrid.Items.Refresh();
        }

        private void UpdateTask(object sender, EventArgs e)
        {
            var updatedTask = TaskPopup.GeneratedTask;
            if (updatedTask == null) return;
            _currentUserStory.AssociatedTasks.Remove(TaskPopup.InitialSource);
            _currentUserStory.AssociatedTasks.Add(updatedTask);
            _sprintManagementService.UpdateUserStory(_currentUserStory);
            userStoryTasksGrid.Items.Refresh();
        }

        private void Row_DoubleClick(object sender, RoutedEventArgs e)
        {
            TaskPopup.Ok -= new EventHandler(CreateTask);
            TaskPopup.Ok += new EventHandler(UpdateTask);
            TaskPopup.InitialSource = userStoryTasksGrid.SelectedItem as TaskDto;
            TaskPopup.IsOpen = !TaskPopup.IsOpen;
        }
    }
}
