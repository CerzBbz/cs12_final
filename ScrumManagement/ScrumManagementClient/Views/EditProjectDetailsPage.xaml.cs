﻿using ScrumManagementClient.Navigation;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for EditProjectDetailsPage.xaml
    /// </summary>
    public partial class EditProjectDetailsPage : UserControl
    {

        ProjectDto currentProject = CurrentProject.getProject();

        public EditProjectDetailsPage()
        {
            InitializeComponent();

            editedProject.Content += currentProject.Title;
            

        }

        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

        private void UserDetails_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }

    }
}
