﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementClient.ProjectManagementService;
using System.Text.RegularExpressions;
﻿using ScrumManagementClient.Navigation;
using System;

namespace ScrumManagementClient.Views
{

    /// <summary>
    /// Interaction logic for ProductBacklogPage.xaml
    /// </summary>
    public partial class ProductBacklogPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private ProjectDto project;
        private int originalStoriesCount;
        List<UserStoryDto> userStories;

        private int rowEditing = -1;
        private int index;

        public ProductBacklogPage(ProjectDto project)
        {
            InitializeComponent();

            //set up edit and confirm edit buttons so only one is visible
            EditBtn.Visibility = Visibility.Visible;
            ConfirmEditBtn.Visibility = Visibility.Hidden;

            // Sets width of the last column to mac width (fills rest of data grid)
            Loaded += (s, e) => dataGrid.Columns[2].Width = new DataGridLength(1, DataGridLengthUnitType.Star);

            EditBtn.ToolTipOpening += new ToolTipEventHandler(editToolTipOpen);

            dataGrid.CanUserDeleteRows = false;
            originalStoriesCount = dataGrid.Items.Count;

            this.project = project;
            loadBacklog();
            CheckUserPermissions();
        }

        private void CheckUserPermissions()
        {
            UserDto user = UserDetails.getUser();
            if (user.Id != project.OwnerId)
            {
                TitleTxt.Visibility = Visibility.Hidden;
                TitleLbl.Visibility = Visibility.Hidden;
                SummaryTxt.Visibility = Visibility.Hidden;
                SummaryLbl.Visibility = Visibility.Hidden;
                PriorityTxt.Visibility = Visibility.Hidden;
                PriorityLbl.Visibility = Visibility.Hidden;
                AddStoryBtn.Visibility = Visibility.Hidden;
                EditBtn.Visibility = Visibility.Hidden;
                ConfirmEditBtn.Visibility = Visibility.Hidden;
                //DeleteBtn.Visibility = Visibility.Hidden;
            }
        }

        private void loadBacklog()
        {
            dataGrid.ItemsSource = null;

            //make sure no row can be edited
            rowEditing = -1;

            //populate the datagrid with the users stories for the current project
            userStories = _projectManagementService.GetAllUserStoriesForProject(project).ToList();
            userStories = userStories.OrderBy(o => o.Priority).ToList();
            dataGrid.ItemsSource = userStories;


        }

        #region ButtonHandlers

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedIndex != -1)
            {
                rowEditing = dataGrid.SelectedIndex;
                EditBtn.Visibility = Visibility.Hidden;
                ConfirmEditBtn.Visibility = Visibility.Visible;
                dataGrid.IsReadOnly = false;
            }
            else
            {                
                                
            }
        }
        private void ConfirmEditBtn_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Are you sure you wish to edit this user story?","Confirm Edit", MessageBoxButton.YesNoCancel);            

            if (result == MessageBoxResult.Yes)
            {//changes are ready to be pushed to database

                userStories = dataGrid.ItemsSource.AsQueryable().Cast<UserStoryDto>().ToList();

                //if user stories were edited
                if (project.Stories != userStories)
            {
                    project.Stories = userStories;

                    _projectManagementService.UpdateProject(project);
                }

                EditBtn.Visibility = Visibility.Visible;
                ConfirmEditBtn.Visibility = Visibility.Hidden;
                dataGrid.IsReadOnly = true;

                loadBacklog();
            }
            else if (result == MessageBoxResult.No)
            {//changes arent complete
                
            }
            else
            {//cancel all changes

                EditBtn.Visibility = Visibility.Visible;
                ConfirmEditBtn.Visibility = Visibility.Hidden;
                dataGrid.IsReadOnly = true;

                //set user stories back to original
                loadBacklog();
            }

        }

        private UserStoryDto createUserStoryDto(int index)
        {
            //pull out data in data grid and create a UserStoryDto with the data
            UserStoryDto story = new UserStoryDto()
            {
                //Id = userStory.Id;
                //Title = userStory.Title;
                //Summary = userStory.Title;
                //Priority = userStory.Priority;
                //ImpactedArea = userStory.ImpactedArea;
                //StoryPointEstimate = userStory.StoryPointEstimate;
                //AssociatedTasks = userStory.AssociatedTasks.Select(t => new TaskDto(t)).ToList();
            };
            return story;
        }   

        private void ViewBurndownChart(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BurndownPage());
        }

        private void ViewTeam_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        private void ViewSprintBacklog_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SprintBacklogPage(null,null));
        }        

        #endregion
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }     

        private void AddStoryBtn_Click(object sender, RoutedEventArgs e)
        {
            //checks to make sure empty titles or summarys arent added
            if (TitleTxt.Text.Equals(string.Empty))
                MessageBox.Show("Please enter a title");
            else if (SummaryTxt.Text.Equals(string.Empty))
                MessageBox.Show("Please enter a summary");
            else if (PriorityTxt.Equals(string.Empty))
                MessageBox.Show("please enter a priotiy");
            else if (!Regex.IsMatch(PriorityTxt.Text, @"^\d+$")) // check if priority is a digit
                MessageBox.Show("Please enter digits only");
            else
            {
                //TODO(Michael) allow a user to enter impacted area, story point estimates and associated tasks?
                UserStoryDto story = new UserStoryDto()
                {
                    Title = TitleTxt.Text,
                    Summary = SummaryTxt.Text,
                    Priority = int.Parse(PriorityTxt.Text),
                    ImpactedArea = "all",
                    StoryPointEstimate = 4,
                    AssociatedTasks = new List<TaskDto>()
                };

                //add user story to project in database
                _projectManagementService.AddUserStoryToProject(project, story);
                //add story to list of stories in current project so that dataGrid can be updated with just added story              
                List<UserStoryDto> tempStories = project.Stories.ToList();
                tempStories.Add(story);
                project.Stories = tempStories;                

                //wipe the text boxes
                TitleTxt.Text = string.Empty;
                SummaryTxt.Text = string.Empty;
                PriorityTxt.Text = string.Empty;

                //reload backlog
                loadBacklog();
            }

        }

        private void PriorityTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(PriorityTxt.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers");
                PriorityTxt.Text = string.Empty;
            }
        }

        private void editToolTipOpen(object sender, ToolTipEventArgs e)
        {
            if (dataGrid.SelectedIndex == -1)
            {
                editTTBlock.Text = "Please selecte a user story to edit";
            }
            else
            {
                editTTBlock.Text = "";
            }
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {

            var result = MessageBox.Show("Are you sure you wish to delete","Confirm", MessageBoxButton.YesNoCancel);  
            userStories = dataGrid.ItemsSource.AsQueryable().Cast<UserStoryDto>().ToList();
            userStories.Remove(userStories[dataGrid.SelectedIndex]);

            //if user story was deleted
            if (project.Stories != userStories)
            {
                project.Stories = userStories;

                _projectManagementService.UpdateProject(project);
               
                loadBacklog();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }

        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

    }
}
