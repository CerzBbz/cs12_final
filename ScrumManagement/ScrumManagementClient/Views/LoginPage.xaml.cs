﻿using ScrumManagementClient.LoginService;
using System.Windows;
using System.Windows.Controls;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Encryption;
using ScrumManagementClient.Navigation;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : UserControl
    {
        public LoginPage()
        {
            InitializeComponent();
            Application.Current.MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private readonly ILoginService _loginService = new LoginServiceClient();

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {

            var salt = string.Empty;
            if (_loginService.IsUserNameValid(Username.Text))
            {
                var user = new User()
                {
                    Username = Username.Text
                };
                salt = _loginService.GetSalt(user);
            }

            //use SecureString to pin the password, encrypt it when its not being accessed
            var ssPassword = Password.SecurePassword;
            ssPassword.MakeReadOnly();

            var hashSaltPass = Encryption.GenerateSha256Hash(ssPassword, salt);
            
            //dispose and zero over password
            ssPassword.Dispose();

            var result = _loginService.AuthenticateUserDetails(Username.Text, hashSaltPass, salt);
            if (result.Equals(LoginReturnCode.SuccessfulLogin))
            {
                UserDetails.setUser(Username.Text);
                //UserDetails.setUsername(Username.Text);
                //show overview page if log in is sucessfully
                Switcher.Switch(new OverviewPage());
            }
            else
            {
                IncorrectUsernameLbl.Visibility = Visibility.Visible;
                Password.Clear();
            }

        }

        private void registerButton_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new RegistrationPage());
        }
    }
}
