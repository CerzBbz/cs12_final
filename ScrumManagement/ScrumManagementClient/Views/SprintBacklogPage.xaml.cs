﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ScrumManagementClient.ProjectManagementService;
using IProjectManagementService = ScrumManagementClient.ProjectManagementService.IProjectManagementService;
using ScrumManagementClient.Properties;
using ScrumManagementClient.SprintManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for SprintBacklogPage.xaml
    /// </summary>
    public partial class SprintBacklogPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private readonly ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();
        private List<SprintDto> projectSprintList;
        private ProjectDto selectedProject;
        private SprintDto selectedSprint;
        public SprintBacklogPage(ProjectDto project, SprintDto sprint)
        {
            InitializeComponent();

            selectedProject = project;
            selectedSprint = sprint;
            /*
            If a project was sent into the page
            Get a list of it's associated sprints
            Display the title in the 'sprintList' box on the left hand side of the page
            Then, Select the first element of the list which will invoke the 'sprintList_SelectionChanged' method
            */
            if (project == null) { return; }
            projectSprintList = _projectManagementService.GetSprintsFromProject(selectedProject).ToList();
            sprintList.ItemsSource = projectSprintList;
            sprintList.DisplayMemberPath = "Title";

            if(sprint == null) {
                sprintList.SelectedIndex = 0;
                return;
            }

            int i = 0;
            foreach (SprintDto s in projectSprintList)
            {
                if (s.Id == sprint.Id)
                {
                    break;
                }
                i++;
            }
            sprintList.SelectedIndex = i;

        }

        private void sprintList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*
            Get the SelectedIndex, which will then be used to find the specific SprintDto that is selected.
            This Dto is then used to get all of the associated user stories
            If there are user stories we will populate sprintBacklog box
            */
            int sprintIndex = sprintList.SelectedIndex;
            var sprintUserStories = _sprintManagementService.GetUserStoriesFromSprint(projectSprintList[sprintIndex]).ToList();
            if (sprintUserStories.Count <= 0) { return; }
            sprintBacklog.ItemsSource = sprintUserStories;
        }

        private void ManageSprintBacklogBtn_OnClick(object sender, RoutedEventArgs e)
        {
            int sprintIndex = sprintList.SelectedIndex;
            Switcher.Switch(new SprintBacklogManagerPage(selectedProject, projectSprintList[sprintIndex]));
        }
    }
}
