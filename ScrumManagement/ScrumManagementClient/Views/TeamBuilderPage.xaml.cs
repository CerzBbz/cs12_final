﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ScrumManagementClient.ProjectManagementService;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TeamBuilderPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        readonly bool _loaded;

        public TeamBuilderPage()
        {
            InitializeComponent();
            _loaded = true;
        }



        private void teamList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }    

        #region ButtonHandlers        
        /*
        HomeBtn_Click navigates the user to the OverViewPage
        */
        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

        /*
        UserBtn_Click navigates the user to the UserDetailsPage
        */
        private void UserBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }

        /*
        viewTeamBtn_Click navigates the user to the ViewProjectTeamPage
        */
        private void viewTeamBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        /*
        logoutBtn_Click navigates the user to the LoginPage
        */
        private void LogoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }
 
        private void ViewBurndownChart(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BurndownPage());
        }

        private void ViewTeam_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        private void ViewSprintBacklog_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SprintBacklogPage(null, null));
        }
        #endregion

        private void projectCBox_SelectionChanged(object sender, EventArgs e)
        {

        }

        /*
            performSearch() allows the user to search the database for a user by their name. 
            po, dev and sm are flags that check if the user meets those specific roles.
            Once the search has been performed we store the results in var search.
            We then iterate through search, and for each user get their name and store it in a row within the datatable, 
            once all users within search have been added as rows to the DataTable, the DataTable will then be used to
            populate the DataGrid.
        */
        private void PerformSearch()
        { 
            if (_loaded)
            {
                var emailParam = EmailSearchTBox.Text;
                var skillParam = SkillSearchTBox.Text;
                var po = ProductOwnerCBox.IsChecked.Value;
                var dev = DeveloperCBox.IsChecked.Value;
                var sm = ScrumMasterCBox.IsChecked.Value;
                var dt = new DataTable();
                dt.Columns.Add("Username");
                dt.Columns.Add("Skills");

                var searchResults = _projectManagementService.GetUsernamesFromSearch(emailParam, skillParam, po, dev, sm);
                if (searchResults.Any())
                {
                    var skills = _projectManagementService.GetSkillsFromUsernames(searchResults);
                    for (var i = 0; i < searchResults.Length; i++)
                    {
                        var newRow = dt.NewRow();
                        newRow["Username"] = searchResults[i];
                        newRow["Skills"] = skills[i];
                        dt.Rows.Add(newRow);
                    }
                }

                SearchResults.ItemsSource = dt.DefaultView;
            }
        }

        /*
Checking the productOwnerCBOx runs the performSearch() method
*/
        private void productOwnerCBox_Checked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        /*
Checking the developerCBox runs the performSearch() method
*/
        private void developerCBox_Checked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        /*
Checking the scrumMasterCBox runs the performSearch() method
*/
        private void scrumMasterCBox_Checked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        /*
Unchecking the productOwnerCBOx runs the performSearch() method
*/
        private void productOwnerCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        /*
Unchecking the developerCBox runs the performSearch() method
*/
        private void developerCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        /*
Unchecking the scrumMasterCBox runs the performSearch() method
*/
        private void scrumMasterCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        private void searchResults_DblClick(object sender, MouseButtonEventArgs e)
        { 
        }

        private void emailSearchTBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            PerformSearch();
        }

        private void skillSearchTBox_TextChanged(object sender, TextChangedEventArgs e)
        { 
            PerformSearch();
        }

        private void AddUserBtn_Click(object sender, RoutedEventArgs e)
        {
            //Get UserID of user of selected row
            //Get selected SprintID
            //Make row in Sprint_Member using SprintID and UserID
        }
    }
}