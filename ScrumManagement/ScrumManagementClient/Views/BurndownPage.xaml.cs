﻿using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for BurndownPage.xaml
    /// </summary>
    public partial class BurndownPage : UserControl
    {
        public BurndownPage()
        {
            InitializeComponent();

            TaskDto t = new TaskDto();
            t.EstimatedHours = 210;
            // us.AssociatedTasks.Add(t);
            // sp.Stories.Add(us);           

            ((LineSeries)mcChart.Series[0]).ItemsSource = new KeyValuePair<DateTime, int>[]{
                    new KeyValuePair<DateTime,int>(DateTime.Now, t.EstimatedHours),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(1), 190),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(2), 150),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(3), 90),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(4),45) };
        }


        #region ButtonHandlers
        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

        private void ViewBurndownChart(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BurndownPage());
        }

        private void ViewTeam_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        private void ViewSprintBacklog_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SprintBacklogPage(null, null));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }
        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((LineSeries)mcChart.Series[1]).ItemsSource = new KeyValuePair<DateTime, int>[]{
                    new KeyValuePair<DateTime,int>(DateTime.Now, 100),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(1), 80),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(2), 65),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(3), 30),
                    new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(4),5) };
        }
    }
}
