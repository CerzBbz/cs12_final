﻿using ScrumManagementClient.ProjectManagementService;
using ScrumManagementClient.Properties;
using ScrumManagementServer.DataContracts.DTO_classes;
﻿using ScrumManagementClient.Navigation;
using ScrumManagementServer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScrumManagementClient.SprintManagementService;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for OverviewPage.xaml
    /// </summary>
    public partial class OverviewPage : UserControl
    {
        private IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();
        private List<ProjectDto> projectList;
        private ProjectDto selectedProject = null;
        private SprintDto selectedSprint = null;
        
        public OverviewPage()
        {
            InitializeComponent();
            
            //TODO(michael/jack) change from get every project to just get related to the logged in user
            projectList = _projectManagementService.GetAllProjects().ToList();
            
            LoggedInUser.Content = UserDetails.getUser().Username;
            UserDto user = UserDetails.getUser();
            

            //if there is a project related to usetr display in activeProjectLIsts, ListBox
            if (projectList.Count > 0)
            {
                activeProjectsList.ItemsSource = projectList;
                //display only the title of each project
                activeProjectsList.DisplayMemberPath = "Title";

                //set default selected index to 0 and calls activeProjectList_Selection Changed
                activeProjectsList.SelectedIndex = 0;
            }

            //if there are no projects set the viewProductBacklogBtn as invisible
            if (activeProjectsList.Items.Count == 0)
            {
                viewBtn.Visibility = Visibility.Hidden;
            }
            else
            {
                viewBtn.Visibility = Visibility.Visible;
            }

           
        }

       

        /// <summary>
        /// This checks the user roles of the currently logged in user, at the moment only checks if they are a developer
        /// Sets visibility of edit product backlog button to hidden
        /// </summary>
        /// <param name="ur"></param>
        public void performUserAccessCheck(List<UserRoleDto> ur)
        {
            ////This stuff is in the wrong place.
            ////if user has a role called Product Owner
            //if (!ur.Exists(a => a.RoleDescription.Equals("Product Owner")))
            //{
            //    //Button will be made invisible
            //    editBtn.Visibility = Visibility.Hidden;
            //    //The label text will be moved so the UI doesnt look strange without the button.
            //    label1.Margin = new Thickness(label1.Margin.Left, (label1.Margin.Top + 20), label1.Margin.Right, label1.Margin.Bottom);
            //}
        }

        //rename all button actions to fit standard formatting

        #region ButtonHandlers        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new NewProjectPage());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }
        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ProductBacklogPage(projectList[activeProjectsList.SelectedIndex]));
        }

        private void ViewBurndownChart(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BurndownPage());
        }

        private void ViewTeam_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        private void ViewSprintBacklog_Click(object sender, RoutedEventArgs e)
        {
            if (selectedProject != null)
            {
                if (selectedSprint != null)
                {
                    Switcher.Switch(new SprintBacklogPage(selectedProject, selectedSprint));
                }
                else
                {
                    MessageBox.Show("Please select a Sprint");
                }
            }
            else { MessageBox.Show("Please select a Project and a Sprint"); }
        }

        private void viewTeamPageBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }

        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }
        #endregion


        /// <summary>
        /// Gets the current context for project and navigates to new sprint page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createSprintBtn_Click(object sender, RoutedEventArgs e)
        {
           
            selectedProject = activeProjectsList.SelectedItem as ProjectDto;

            CurrentProject.setProject(selectedProject.Title);

            //activeProjectsList.SelectedItem
            Switcher.Switch(new NewSprintPage());
        }

        /// <summary>
        /// Checks that a valid selection is made in the project list box and performs UI updates with relevant project info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActiveProjectsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            selectedProject = activeProjectsList.SelectedItem as ProjectDto;
            SprintOverviewDisabled.Visibility = Visibility.Hidden;

            CurrentProject.setProject(selectedProject.Title);

            //create reference for selected project 
            int projectIndex = activeProjectsList.SelectedIndex;
            //selecting all user stories based on the selected project 
            //TODO project management service isnt returning any list, need to change "Stories" in Project
            List<UserStoryDto> userStoryList = _projectManagementService.GetAllUserStoriesForProject(projectList[projectIndex]).ToList();

            //add user stories to list box, and display "Title" of the user stories
            if (userStoryList.Count > 0)
            {
                productBacklogList.ItemsSource = userStoryList;
                productBacklogList.DisplayMemberPath = "Title";
            }
            else
            {
                //if no user stories for a projects dont display any
                productBacklogList.ItemsSource = "";
            }

            updateProjectDetailsLabels();
            UpdateSprintOverview();
        }     

       private void updateProjectDetailsLabels(){
            projectManagerLbl.Content = selectedProject.Manager.Username ?? "-";
            productOwnerLbl.Content = selectedProject.Owner.Username ?? "-";
            projectDescriptionLbl.Content = selectedProject.Description ?? "-";
            // developersLbl.Content = selectedProject.Sprints.
            projectDescriptionLbl.Content = selectedProject.Description;
           
        }

       private void ProjectListBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
           selectedProject = activeProjectsList.SelectedItem as ProjectDto;
           updateProjectDetailsLabels();
        }



        private void UpdateSprintOverview()
        {
            var projectSprintList = _projectManagementService.GetSprintsFromProject(selectedProject).ToList();
            SprintListBox.ItemsSource = projectSprintList;
            SprintListBox.DisplayMemberPath = "Title";
            SprintListBox.SelectedIndex = 0;
            UpdateSprintLabels();
        }

        private void UpdateSprintLabels()
        {
            selectedSprint = SprintListBox.SelectedItem as SprintDto;
            if (selectedSprint != null)
            {

                ScrumMasterLbl.Content = selectedSprint.ScrumMaster.Username;
                SprintStartLbl.Content = selectedSprint.StartDate.ToShortDateString();
                SprintEndLbl.Content = selectedSprint.EndDate.ToShortDateString();
                TotalStoriesLbl.Content = selectedSprint.Stories.Count;
            }
            else
            {
                ScrumMasterLbl.Content = "-";
                SprintStartLbl.Content = "-";
                SprintEndLbl.Content = "-";
                TotalStoriesLbl.Content = "-";
            }
            /**
            Need to do tasks:
            */
            //TaskCompletedLbl;
            //TaskInProgressLbl;
            //TasksNotStartedLbl;
        }

        private void SprintListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            selectedSprint = SprintListBox.SelectedItem as SprintDto;
            if(CurrentSprint.getSprint()!=null)
            CurrentSprint.setSprint(selectedSprint.Id);
            UpdateSprintLabels();
        }

        private void EditProject(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new EditProjectDetailsPage());
        }
    }
}
