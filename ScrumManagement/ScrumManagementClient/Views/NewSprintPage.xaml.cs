﻿using CustomControlLibrary;
using ScrumManagementClient.Navigation;
using ScrumManagementClient.ProjectManagementService;
using ScrumManagementClient.SprintManagementService;
using ScrumManagementClient.TaskManagementService;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IProjectManagementService = ScrumManagementClient.ProjectManagementService.IProjectManagementService;

namespace ScrumManagementClient.Views
{
    /// <summary>
    /// Interaction logic for SprintBacklogPage.xaml
    /// </summary>
    public partial class NewSprintPage : UserControl
    {
        private readonly IProjectManagementService _projectManagementService = new ProjectManagementServiceClient();
        private readonly ISprintManagementService _sprintManagementService = new SprintManagementServiceClient();
        private readonly ITaskManagementService _taskManagementService = new TaskManagementServiceClient();

        public NewSprintPage()
        {
            InitializeComponent();
            SetupUserRoles();

            var developers = _projectManagementService.GetUsersWithRole("Developer");
            foreach (var dev in developers)
            {
                DevelopersListBox.Items.Add(dev);
            }
            

            projectselected.Content +=  CurrentProject.getProject().Title;

            SprintStartDatePicker.BlackoutDates.Add(new CalendarDateRange(DateTime.MinValue, DateTime.Now.AddDays(-1)));
            SprintEndDatePicker.BlackoutDates.Add(new CalendarDateRange(DateTime.MinValue, DateTime.Now));
        }

        private void SetupUserRoles()
        {
            foreach (var item in _projectManagementService.GetUsersWithRole("Scrum Master"))
            {
                ScrumMasterCbx.Items.Add(item);
            }
        }

        private void HomeBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new OverviewPage());
        }

        private void CreateSprintBtn_Click(object sender, RoutedEventArgs e)
        {

            UserDto chosenScrumMaster = null;
            DateTime sprintStartDate = DateTime.Now;
            DateTime sprintEndDate = DateTime.Now;
            List<UserDto> selectedDevelopers = new List<UserDto>();

           
            if (ScrumMasterCbx.SelectedItem == null || SprintStartDatePicker.SelectedDate == null || SprintEndDatePicker.SelectedDate == null || DevelopersListBox.SelectedItems == null)
            {
                MessageBox.Show("Please fill in all fields");
            }
            else
            {
                chosenScrumMaster = ScrumMasterCbx.SelectedItem as UserDto;
                sprintStartDate = (DateTime)SprintStartDatePicker.SelectedDate;
                sprintEndDate = (DateTime)SprintEndDatePicker.SelectedDate;

                //Validation to check end date is after start date
                //int dateCompare = DateTime.Compare(sprintEndDate, sprintStartDate);

                //if (dateCompare<0)
                //{
                //    MessageBox.Show("Set start date before end date");
                //}
                //else
                //{
                //    sprintStartDate = (DateTime)SprintStartDatePicker.SelectedDate;
                //    sprintEndDate = (DateTime)SprintEndDatePicker.SelectedDate;
                //}
                

                foreach (var dev in DevelopersListBox.SelectedItems)
                {
                    selectedDevelopers.Add(dev as UserDto);
                }

                var proj = CurrentProject.getProject();

                _sprintManagementService.CreateSprint(proj, SprintTitle.Text, chosenScrumMaster.Id, sprintStartDate, sprintEndDate, selectedDevelopers);

                Switcher.Switch(new OverviewPage());

            }
             
            
            
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserDetailsPage());
        }
        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new LoginPage());
        }
        private void ViewBurndownChart(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BurndownPage());
        }
        private void ViewTeam_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ViewProjectTeamPage());
        }
        private void ViewSprintBacklog_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SprintBacklogPage(null, null));
        }

        // add code to remove an entry from the backlog
        private void RemoveFromSprintBacklog_Click(object sender, RoutedEventArgs e)
        {

        }

        //ToDo: Remove when there's UI to associate functionality too
        
        //ToDo: Remove when there's UI to associate functionality too
        
    }
}
