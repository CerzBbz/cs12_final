﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumManagementServer.Services;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.Entity;
using System.Data.Entity;
using ScrumManagementTests.Mock_Helpers;
using Moq;

namespace ScrumManagementTests.Server_Tests
{
    [TestClass]
    public class SprintTests
    {
        private SprintService _sprintService;
        private UserService _userService;
        private SprintDao _sprintDao;
        private UserDao _userDao;
        private ProjectService _projectService;
        private ProjectDao _projectDao;
        private UserStoryDao _userStoryDao;

        private User _dev1, _scrumMaster1;

        [TestInitialize]
        public void Setup()
        {
            var developerRoles = new List<UserRole> { new UserRole { RoleDescription = "Developer" } };
            var scrumMasterRoles = new List<UserRole> { new UserRole { RoleDescription = "Scrum Master" } };
            
            _dev1 = new User { Username = "test1qub.ac.uk", Password = "reallysecurepassword", UserRoles = developerRoles };
            var dev2 = new User { Username = "test2@qub.ac.uk", Password = "password2", UserRoles = developerRoles };
            var dev3 = new User { Username = "test3@qub.ac.uk", Password = "password2", UserRoles = developerRoles };
            var dev4 = new User { Username = "test4@qub.ac.uk", Password = "password2", UserRoles = developerRoles };
            var dev5 = new User { Username = "test5@qub.ac.uk", Password = "password2", UserRoles = developerRoles };
            _scrumMaster1 = new User { Username = "test11@qub.ac.uk", Password = "password2", UserRoles = scrumMasterRoles };
            var master2 = new User { Username = "test12@qub.ac.uk", Password = "password2", UserRoles = scrumMasterRoles };
            var master3 = new User { Username = "test13@qub.ac.uk", Password = "password2", UserRoles = scrumMasterRoles };
            var userData = new List<User>
            {
                _dev1, dev2,
                dev3, dev4,
                dev5, _scrumMaster1,
                master2, master3
            };

            var sprintData = new List<Sprint>
            {
                new Sprint {Id = 1, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint1", ScrumMaster = _scrumMaster1, Developers = new List<User> { _dev1, dev2, dev3, dev4, dev5 } },
                new Sprint {Id = 2, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint2", ScrumMaster = _scrumMaster1, Developers = new List<User> { dev5, dev2, dev5 } },
                new Sprint {Id = 3, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint3", ScrumMaster = master2, Developers = new List<User> { _dev1 } },
                new Sprint {Id = 4, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint4", ScrumMaster = master2, Developers = new List<User> { dev3, _dev1 } },
                new Sprint {Id = 5, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint5", ScrumMaster = master3, Developers = new List<User> { _scrumMaster1 } },
                new Sprint {Id = 6, EndDate = DateTime.Now, StartDate = DateTime.Now, Title = "sprint6", ScrumMaster = master3, Developers = new List<User> { _scrumMaster1, master2, _dev1, dev2, dev3 } },
            };
            
            var userMockSet = MockHelper.GetMockedDbSet(userData);
            var sprintMockSet = MockHelper.GetMockedDbSet(sprintData);
            
            var mockContext = MockHelper.GetEmptyContext();
            mockContext.Setup(c => c.Users).Returns(userMockSet.Object);
            mockContext.Setup(c => c.Sprints).Returns(sprintMockSet.Object);

            _projectService = new ProjectService(new ProjectDao(new MockScrumContextFactory(mockContext.Object)));
            _sprintService = new SprintService(new SprintDao(new MockScrumContextFactory(mockContext.Object)));
            _userService = new UserService(new UserDao(new MockScrumContextFactory(mockContext.Object)));
        }

        [TestMethod]
        public void SprintGetAllSprints()
        {
           IEnumerable<Sprint> sprints = _sprintService.GetAllSprints();
           Sprint sprint = sprints.Where(a => a.Id == 1).First();

           //Has the same amount of sprints as in the context
           Assert.AreEqual(true, sprints.Count().Equals(6));
        }

        [TestMethod]
        public void SprintCreateNew()
        {
            Sprint sprint = new Sprint {Id = 7, StartDate = DateTime.Now, EndDate = DateTime.Now, Title = "Created", Developers = new List<User>{ _dev1 }, ScrumMaster = _scrumMaster1 };
            _sprintService.CreateSprint(null, sprint);
            IEnumerable<Sprint> sprints = _sprintService.GetAllSprints();
            Sprint sprintReturn = sprints.Where(a => a.Id == 7).First();

            //Should now have 7 sprints
            Assert.AreEqual(true, sprints.Count().Equals(7));
        }

        /*
        [TestMethod]
        public void SprintAddMember()
        {
            Sprint sprint = _sprintService.GetAllSprints().Where(a=>a.Id == 1).FirstOrDefault();
            User addedUser = new User { Id = 8, Username = "JustAdded@test.com", UserRoles = new List<UserRole> { new UserRole { RoleDescription = "Developer"} } };

            _sprintService.AddMemberToSprint(sprint, addedUser);
            Sprint sprintUpdated = _sprintService.GetAllSprints().Where(a => a.Id == sprint.Id).FirstOrDefault();
            Assert.IsTrue(sprintUpdated.Developers.Contains(addedUser));
        }*/

        [TestMethod]
        public void SprintBacklogAddAndRemoveUserStories()
        {
            Sprint sprint = new Sprint
            {
                Title = "Sprint",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now, 
                Id = 500, 
                ScrumMaster = new User{
                    Id = 50,
                    Username = "Robin@James.Test", 
                    UserRoles = new List<UserRole> { new UserRole {RoleDescription = "Developer"} },
                }
            };
            var size = sprint.Stories.Count;
            Console.Out.Write(size);
            var story = new UserStory
            {
                Title = "StoryForSprint",
                ImpactedArea = "Tests",
                Priority = 4,
                Summary = "Adding Story to Sprint",
                StoryPointEstimate = 8
            };
            sprint.Stories.Add(story);
            _sprintService.AddUserStoryToSprint(sprint, story);
            Assert.IsTrue((size+1) == sprint.Stories.Count);
            sprint.Stories.Remove(story);
            _sprintService.RemoveUserStoryFromSprint(sprint, story);
            Assert.IsTrue(size == sprint.Stories.Count);
        }

        [TestMethod]
        public void IsUserstoryEditable()
        {
            var story = new UserStory
            {
                Title = "StoryForSprint",
                ImpactedArea = "Tests",
                Priority = 4,
                Summary = "Adding Story to Sprint",
                StoryPointEstimate = 8,
                Editable = false
            };
            Project project = new Project {
                Title = "TestProject", 
                Manager = new User { 
                    Id = 1,
                    Username = "Manager", 
                    UserRoles = new List<UserRole> { 
                        new UserRole {
                            RoleDescription = "Scrum Master"
                        } 
                    }
                },
                ManagerId = 1,
                Owner  = new User { 
                    Id = 2,
                    Username = "Owner", 
                    UserRoles = new List<UserRole> { 
                        new UserRole {
                            RoleDescription = "Product Owner"
                        } 
                    }
                },
                OwnerId = 2,
                Stories = new List<UserStory> { story }
            };

            Assert.IsTrue(_projectService.UpdateUserStory(project, story) == 0);
        }


    }
}