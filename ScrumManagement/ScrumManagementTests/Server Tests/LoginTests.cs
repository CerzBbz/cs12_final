﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumManagementServer;
using ScrumManagementServer.Dao;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Encryption;
using ScrumManagementServer.Services;
using ScrumManagementTests.Mock_Helpers;

namespace ScrumManagementTests.Server_Tests
{
    [TestClass]
    public class LoginTests
    {
        private ILoginService _loginService;
        private UserDao _userDao;

        [TestInitialize]
        public void Setup()
        {
            var salt1 = Encryption.CreateSalt(32);
            var salt2 = Encryption.CreateSalt(32);
            var salt3 = Encryption.CreateSalt(32);

            var password1 = Encryption.GenerateSha256Hash("validPassword".ToSecureString(), salt1);
            var password2 = Encryption.GenerateSha256Hash("password2".ToSecureString(), salt2);
            var password3 = Encryption.GenerateSha256Hash("password3".ToSecureString(), salt3);

            var userData = new List<User>
            {
                new User { Username = "mh@valid.com", Password = password1, Salt = salt1 },
                new User { Username = "test2@test.com", Password = password2, Salt = salt2 },
                new User { Username = "test3@test.com", Password = password3, Salt = salt3 },
            };
            var userMockSet = MockHelper.GetMockedDbSet(userData);

            var mockContext = MockHelper.GetEmptyContext();
            mockContext.Setup(c => c.Users).Returns(userMockSet.Object);
            
            _userDao = new UserDao(new MockScrumContextFactory(mockContext.Object));
            _loginService = new LoginService(new UserService(_userDao));
        }

        [TestMethod]
        public void LogInWithValidCredentials()
        {
            //correct UN +Pass
            var validUsername = "mh@valid.com";
            var validPass = "validPassword";

            //set up User that is used to get their salt from the database
            var user = new User
            {
                Username = validUsername
            };
            //get salt belonging to particular user
            var salt = _loginService.GetSalt(user);

            //hash the password and salt
            var hashSaltPass = Encryption.GenerateSha256Hash(validPass.ToSecureString(), salt);

            var result = _loginService.AuthenticateUserDetails(validUsername, hashSaltPass, salt);
            var expectedResult = LoginReturnCode.SuccessfulLogin;

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void LogInWithInvalidPassword()
        {
            //correct UN + incorrect pass
            var validUsername = "mh@valid.com";
            var invalidPass = "invalidPass";

            //set up User that is used to get their salt from the database
            var user = new User
            {
                Username = validUsername
            };
            //get salt belonging to particular user
            var salt = _loginService.GetSalt(user);

            //hash the password and salt
            var hashSaltPass = Encryption.GenerateSha256Hash(invalidPass.ToSecureString(), salt);

            var result = _loginService.AuthenticateUserDetails(validUsername, hashSaltPass, salt);
            var expectedResult = LoginReturnCode.InvalidLoginCredentials;

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void LogInWithInvalidUsername()
        {
            //incorrect UN + correct pass
            //correct UN +Pass
            var invalidUsername = "mh@invalid.com";
            var validPass = "validPass";

            //set up User that is used to get their salt from the database
            var user = new User
            {
                Username = invalidUsername
            };
            //get salt belonging to particular user
            var salt = _loginService.GetSalt(user);

            //hash the password and salt
            var hashSaltPass = Encryption.GenerateSha256Hash(validPass.ToSecureString(), salt);

            var result = _loginService.AuthenticateUserDetails(invalidUsername, hashSaltPass, salt);
            var expectedResult = LoginReturnCode.InvalidLoginCredentials;

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void LogInWithInalidCredentials()
        {
            //incorrect UN + incorrect pass
            //correct UN +Pass
            var invalidUsername = "mh@invalid.com";
            var invalidPass = "invalidPass";

            //set up User that is used to get their salt from the database
            var user = new User
            {
                Username = invalidUsername
            };
            //get salt belonging to particular user
            var salt = _loginService.GetSalt(user);

            //hash the password and salt
            var hashSaltPass = Encryption.GenerateSha256Hash(invalidPass.ToSecureString(), salt);

            var result = _loginService.AuthenticateUserDetails(invalidUsername, hashSaltPass, salt);
            Console.WriteLine(result);
            var expectedResult = LoginReturnCode.InvalidLoginCredentials;

            Assert.AreEqual(expectedResult, result);
        }
    }
}