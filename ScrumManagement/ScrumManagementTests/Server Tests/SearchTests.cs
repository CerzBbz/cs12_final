﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumManagementServer;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Services;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementTests.Mock_Helpers;

namespace ScrumManagementTests.Server_Tests
{
    [TestClass]
    public class SearchTests
    {
        private IProjectManagementService _projectManagementService;
        private UserDao _userDao;
        private ProjectDao _projectDao;
        private UserService _userService;
        private ProjectService _projectService;
        private SprintService _sprintService;
        private SprintDao _sprintDao;
        private UserStoryDao _userStoryDao;
        private UserStoryService _userStoryService;

        private readonly UserRole _productOwnerRole = new UserRole { RoleDescription = "Product Owner" };
        private readonly UserRole _scrumMasterRole = new UserRole { RoleDescription = "Scrum Master" };
        private readonly UserRole _developerRole = new UserRole { RoleDescription = "Developer" };

        [TestInitialize]
        public void Setup()
        {
            var mockContext = MockHelper.GetEmptyContext();

            _userDao = new UserDao(new MockScrumContextFactory(mockContext.Object));
            _userService = new UserService(_userDao);

            _projectDao = new ProjectDao(new MockScrumContextFactory(mockContext.Object));
            _projectService = new ProjectService(_projectDao);

            _sprintDao = new SprintDao(new MockScrumContextFactory(mockContext.Object));
            _sprintService = new SprintService(_sprintDao);

            _userStoryDao = new UserStoryDao(new MockScrumContextFactory(mockContext.Object));
            _userStoryService = new UserStoryService(_userStoryDao, null);

            _projectManagementService = new ProjectManagementService(_projectService, _userService, _sprintService, _userStoryService);
        }

        /*
        [TestMethod]
        public void SearchForProductOwner()
        {
            var productOwnersToAdd = new List<User>{
                new User {Username = "test@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "productOwner@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "master@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_scrumMasterRole, _developerRole}},
                new User {Username = "test2@qub.ac.uk", Password = "password2", Skills="Java", UserRoles = {_productOwnerRole}}
            };
            var email = "te";
            var matchingProductOwners = productOwnersToAdd.Where(po => po.Username.Contains(email) && po.IsProductOwner()).Select(po => po.Username).ToList();

            productOwnersToAdd.ForEach(po => _userDao.Add(po));
            var matches = _userService.GetUsernamesFromSearch(email, "", true, false, false);
            CollectionAssert.AreEquivalent(matchingProductOwners, matches);
        }

        [TestMethod]
        public void SearchForScrumMaster()
        {
            var scrumMastersToAdd = new List<User>{
                new User {Username = "scrum@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_scrumMasterRole}},
                new User {Username = "productOwner@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "scrumMaster@qub.ac.uk", Password = "password2", Skills="Java", UserRoles = {_scrumMasterRole}}
            };
            var email = "scrum";
            var matchingScrumMasters = scrumMastersToAdd.Where(sm => sm.Username.Contains(email) && sm.IsScrumMaster()).Select(sm => sm.Username).ToList();

            scrumMastersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch(email, "", false, false, true);
            CollectionAssert.AreEquivalent(matchingScrumMasters, matches);
        }

        [TestMethod]
        public void SearchForDeveloper()
        {
            var developersToAdd = new List<User>{
                new User {Username = "dev@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_developerRole}},
                new User {Username = "newstartdev@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_developerRole}},
                new User {Username = "newstartpo@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "scrumMaster@qub.ac.uk", Password = "password2"}
            };
            var email = "new";
            var matchingDevelopers = developersToAdd.Where(dev => dev.Username.Contains(email) && dev.IsDeveloper()).Select(dev => dev.Username).ToList();

            developersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch(email, "", false, true, false);
            CollectionAssert.AreEquivalent(matchingDevelopers, matches);
        }

        [TestMethod]
        public void SearchForProductOwnerAndScrumMaster()
        {
            var usersToAdd = new List<User>{
                new User {Username = "dev@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_developerRole}},
                new User {Username = "newstartdev@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_developerRole}},
                new User {Username = "newstartpo@qub.ac.uk", Password = "reallysecurepassword2", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "scrumMaster@qub.ac.uk", Password = "password2", Skills="Java",},
                new User {Username = "newMaster@qub.ac.uk", Password = "password22", Skills="Java", UserRoles = {_productOwnerRole, _scrumMasterRole}}
            };
            var email = "new";
            var matchingUsers = usersToAdd.Where(u => u.Username.Contains(email) && (u.IsProductOwner() || u.IsScrumMaster())).Select(u => u.Username).ToList();

            usersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch(email, "", true, false, true);
            CollectionAssert.AreEquivalent(matchingUsers, matches);
        }

        [TestMethod]
        public void SearchForProductOwnerScrumMasterAndDeveloper()
        {
            var usersToAdd = new List<User>{
                new User {Username = "dev@qub.ac.uk", Password = "reallysecurepassword",Skills="Java",  UserRoles = {_developerRole}},
                new User {Username = "newstartdev@qub.ac.uk", Password = "reallysecurepassword", Skills="Java", UserRoles = {_developerRole}},
                new User {Username = "newstartpo@qub.ac.uk", Password = "reallysecurepassword2", Skills="Java", UserRoles = {_productOwnerRole}},
                new User {Username = "scrumMaster@qub.ac.uk", Password = "password2", Skills="Java", UserRoles = {_scrumMasterRole}},
                new User {Username = "newMaster@qub.ac.uk", Password = "password22", Skills="Java", UserRoles = {_developerRole, _scrumMasterRole, _productOwnerRole}}
            };
            var email = "new";
            var matchingUsers = usersToAdd.Where(u => u.Username.Contains(email) && (u.IsProductOwner() || u.IsScrumMaster() || u.IsDeveloper())).Select(u => u.Username).ToList();

            usersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch(email, "", true, true, true);
            CollectionAssert.AreEquivalent(matchingUsers, matches);
        }

        [TestMethod]
        public void SearchForUserBySkills()
        {
            var usersToAdd = new List<User>
            {
                new User
                {
                    Username = "dev@qub.ac.uk",
                    Password = "reallysecurepassword",
                    Skills = "Java",
                    UserRoles = {_developerRole}
                },
                new User
                {
                    Username = "newstartdev@qub.ac.uk",
                    Password = "reallysecurepassword",
                    Skills = "C#,PHP,Java",
                    UserRoles = {_developerRole}
                },
                new User
                {
                    Username = "newstartpo@qub.ac.uk",
                    Password = "reallysecurepassword2",
                    Skills = "C++",
                    UserRoles = {_productOwnerRole}
                },
                new User
                {
                    Username = "scrumMaster@qub.ac.uk",
                    Password = "password2",
                    Skills = "None",
                    UserRoles = {_scrumMasterRole}
                },
                new User
                {
                    Username = "newMaster@qub.ac.uk",
                    Password = "password22",
                    Skills = "Unit testing, Java",
                    UserRoles = {_developerRole, _scrumMasterRole, _productOwnerRole}
                }
            };
            var skills = "Java";
            var matchingUsers = usersToAdd.Where(u => u.Skills.Contains(skills)).ToList();

            List<String> matchingUsernames = new List<String>();

            foreach (var item in matchingUsers)
            {
                matchingUsernames.Add(item.Username);
            }


            //matching users is a user object, matches is a string

            usersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch("", skills, true, true, true);
            CollectionAssert.AreEquivalent(matchingUsernames, matches);
        }*/

        /*
        [TestMethod]
        public void SearchBySkills()
        {
            var usersToAdd = new List<User>{
                new User {Username = "noskill@qub.ac.uk", Skills = "",Password = "reallysecurepassword", UserRoles = {_developerRole}},
                new User {Username = "javaandc@qub.ac.uk", Skills = "java, c", Password = "reallysecurepassword", UserRoles = {_developerRole}},
                new User {Username = "justjava@qub.ac.uk", Skills = "java", Password = "reallysecurepassword2", UserRoles = {_developerRole, _productOwnerRole}},
                new User {Username = "javaandc@qub.ac.uk", Skills = "java", Password = "password2", UserRoles = {_developerRole}}
            };
            var skill = "java";
            var matchingUsers = usersToAdd.Where(u => u.Skills.Contains(skill)).Select(u => u.Username).ToList();

            usersToAdd.ForEach(sm => _userDao.Add(sm));
            var matches = _userService.GetUsernamesFromSearch("", skill, true, true, true);
            CollectionAssert.AreEquivalent(matchingUsers, matches);
        }*/
    }
}
