﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumManagementServer;
using ScrumManagementServer.Dao;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Encryption;
using ScrumManagementServer.Services;
using ScrumManagementTests.Mock_Helpers;

namespace ScrumManagementTests.Server_Tests
{
    [TestClass]
    public class RegistrationTests
    {
        private ILoginService _loginService;
        private UserRoleDao _userRoleDao;
        private UserDao _userDao;

        [TestInitialize]
        public void Setup()
        {
            var userData = new List<User>
            {
                new User {Username = "test@qub.ac.uk", Password = "reallysecurepassword"},
                new User {Username = "test2@qub.ac.uk", Password = "password2"},
                new User {Username = "test3@qub.ac.uk", Password = "password3"}
            };
            var userMockSet = MockHelper.GetMockedDbSet(userData);

            var mockContext = MockHelper.GetEmptyContext();
            mockContext.Setup(c => c.Users).Returns(userMockSet.Object);

            _userDao = new UserDao(new MockScrumContextFactory(mockContext.Object));
            _loginService = new LoginService(new UserService(_userDao));
            _userRoleDao = new UserRoleDao(new MockScrumContextFactory(mockContext.Object));
        }

        [TestMethod]
        public void RegistrationWithInvalidUsername()
        {
            var validPassword = "password";
            var emptyString = string.Empty;
            var plainString = "username";
            var incompleteEmail = "username@";
            var validSkills = "Java";

            var salt = Encryption.CreateSalt(32);
            var saltedValidPass = Encryption.GenerateSha256Hash(validPassword.ToSecureString(), salt);

            Assert.AreEqual(LoginReturnCode.InvalidEmailAddress, _loginService.RegisterNewUser(emptyString, saltedValidPass, salt, validSkills));
            Assert.AreEqual(LoginReturnCode.InvalidEmailAddress, _loginService.RegisterNewUser(plainString, saltedValidPass, salt, validSkills));
            Assert.AreEqual(LoginReturnCode.InvalidEmailAddress, _loginService.RegisterNewUser(incompleteEmail, saltedValidPass, salt, validSkills));
        }

        [TestMethod]
        public void RegistrationWithUserNameAlreadyInUse()
        {
            var usernameInUse = "test@qub.ac.uk";
            var validPassword = "password";
            var salt = Encryption.CreateSalt(32);
            var saltedPass = Encryption.GenerateSha256Hash(validPassword.ToSecureString(), salt);
            var validSkills = "Java";

            Assert.AreEqual(LoginReturnCode.UsernameAlreadyExists, _loginService.RegisterNewUser(usernameInUse, saltedPass, salt, validSkills));
        }

        [TestMethod]
        public void RegistrationWithValidUsername()
        {
            var validPassword = "password";
            var username = "username@test.com";
            var salt = Encryption.CreateSalt(32);
            var saltedPass = Encryption.GenerateSha256Hash(validPassword.ToSecureString(), salt);
            var validSkills = "Java";

            Assert.AreEqual(LoginReturnCode.SuccessfulRegistration, _loginService.RegisterNewUser(
                username, saltedPass, salt, validSkills));
        }

        [TestMethod]
        public void RegistrationWithInvalidPassword()
        {
            var validUsername = "testInvalidPass@qub.ac.uk";
            var emptyPassword = string.Empty;
            var shortPassword = "pass";

            var salt = Encryption.CreateSalt(32);

            var saltedEmptyPass = emptyPassword.Length < 8 ? "" :
                                        Encryption.GenerateSha256Hash(emptyPassword.ToSecureString(), salt);
            var saltedShortPass = shortPassword.Length < 8 ? "" :
                                        Encryption.GenerateSha256Hash(shortPassword.ToSecureString(), salt);

            var validSkills = "Java";

            var emptyPassResult = _loginService.RegisterNewUser(validUsername, saltedEmptyPass, salt, validSkills);
            var shortPassResult = _loginService.RegisterNewUser(validUsername, saltedShortPass, salt, validSkills);
            //tests might fail as the encryption process will give the password complexity, might need to change order of complexity check
            Assert.AreEqual(LoginReturnCode.InvalidPasswordComplexity, emptyPassResult);
            Assert.AreEqual(LoginReturnCode.InvalidPasswordComplexity, shortPassResult);
        }

        [TestMethod]
        public void RegistrationWithValidPassword()
        {
            var validUsername = "phollywood04@qub.ac.uk";
            var minimumLengthPassword = "password";
            var salt = Encryption.CreateSalt(32);
            var saltedMinPass = Encryption.GenerateSha256Hash(minimumLengthPassword.ToSecureString(), salt);
            var validSkills = "Java";

            Assert.AreEqual(LoginReturnCode.SuccessfulRegistration, _loginService.RegisterNewUser(validUsername, saltedMinPass, salt, validSkills));
        }

        /*
        [TestMethod]
        public void AddRoleAgainstRegisteringUser()
        {
            var validUserName = "addUR@qub.ac.uk";
            var salt = Encryption.CreateSalt(32);
            var saltedPass = Encryption.GenerateSha256Hash("password".ToSecureString(), salt);
            var validSkills = "Java";

            var roles = new List<UserRole>
            {
                _userRoleDao.GetUserRoleIfExists("Developer"),
                _userRoleDao.GetUserRoleIfExists("Scrum Master")
            };

            Assert.AreEqual(LoginReturnCode.SuccessfulRegistration, _loginService.RegisterNewUser(validUserName, saltedPass, salt, validSkills, roles));
            var registeredUser = _userDao.GetAllUsers().FirstOrDefault(u => u.Username.Equals(validUserName));
            Assert.IsNotNull(registeredUser);
            CollectionAssert.AreEqual(registeredUser.UserRoles as ICollection, roles);
        }*/
    }
}