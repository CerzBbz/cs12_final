﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Encryption;
using ScrumManagementServer.Services;
using ScrumManagementTests.Mock_Helpers;

namespace ScrumManagementTests.Server_Tests
{
    [TestClass]
    public class ProjectTests
    {
        private ProjectService _projectService;
        private ProjectDao _projectDao;
        private UserDao _userDao;

        [TestInitialize]
        public void Setup()
        {
            var userData = new List<User>
            {
                new User {Id = 1, Username = "test@qub.ac.uk", Password = "reallysecurepassword"},
                new User {Id = 1, Username = "test2@qub.ac.uk", Password = "password2"}
            };
            var userMockSet = MockHelper.GetMockedDbSet(userData);

            var projectData = new List<Project>
            {
                new Project {Id = 1, Title = "TestProject", Manager = userData.ElementAt(0), Owner = userData.ElementAt(1)}
            };
            var projectMockSet = MockHelper.GetMockedDbSet(projectData);

            var mockContext = MockHelper.GetEmptyContext();
            mockContext.Setup(c => c.Users).Returns(userMockSet.Object);
            mockContext.Setup(c => c.Projects).Returns(projectMockSet.Object);

            _projectDao = new ProjectDao(new MockScrumContextFactory(mockContext.Object));
            _userDao = new UserDao(new MockScrumContextFactory(mockContext.Object));
            _projectService = new ProjectService(_projectDao);
        }

        [TestMethod]
        public void TestCreateProject()
        {
            var project = new Project
            {
                Title = "TestAdd",
            };
            var currentProjects = _projectDao.GetAllProjects();
            _projectService.CreateProject(project);
            var projectsAfterCreate = _projectDao.GetAllProjects();
            Assert.IsTrue(projectsAfterCreate.Count().Equals(currentProjects.Count() + 1));
            Assert.IsTrue(projectsAfterCreate.Contains(project));
        }
        /*
        [TestMethod]
        public void TestProjectManagerRelationshipToProject()
        {
            var project = new Project
            {
                Title = "TestAdd",
            };

            var managerSalt = Encryption.CreateSalt(32);
            var managerSaltedPass = Encryption.GenerateSha256Hash("managerPass".ToSecureString(), managerSalt);

            var manager = new User
            {
                Username = "TestManager",
                Password = managerSaltedPass,
                Salt = managerSalt
            };

            _userDao.Add(manager);
            _projectDao.Add(project);
            
            project.Manager = manager;
            _projectDao.Update(project);
            Assert.AreEqual(project.Manager, manager);
        }

        [TestMethod]
        public void TestProductOwnerRelationshipToProject()
        {
            var project = new Project
            {
                Title = "TestAdd",
            };

            var productOwnerSalt = Encryption.CreateSalt(32);
            var productOwnerSaltedPass = Encryption.GenerateSha256Hash("ownerPass".ToSecureString(), productOwnerSalt);

            var productOwner = new User
            {
                Username = "TestScrumMaster",
                Password = productOwnerSaltedPass,
                Salt = productOwnerSalt
            };
            _userDao.Add(productOwner);
            _projectDao.Add(project);

            project.Owner = productOwner;
            _projectDao.Update(project);
            Assert.AreEqual(project.Owner, productOwner);
        }

        [TestMethod]
        public void TestAddUserStoryToProject()
        {
            var story = new UserStory
            {
                Title = "Story1",
                ImpactedArea = "Integration",
                Priority = 4,
                Summary = "Send transaction message to xyz",
                StoryPointEstimate = 8
            };

            var project = _projectDao.GetAllProjects().First();
            _projectService.AddUserStoryToProject(project, story);

            Assert.IsTrue(project.Stories.Contains(story));
        }*/

        [TestMethod]
        public void ReturnEmptyListOnProjectWithNoUserStories()
        {
            var project = _projectDao.GetAllProjects().First();
            CollectionAssert.AreEqual(Enumerable.Empty<UserStory>().ToList(), _projectService.GetAllUserStoriesForProject(project) as ICollection);
        }

        /*
        [TestMethod]
        public void TestUserStoriesReturnForProject()
        {
            var stories = new List<UserStory>
            {
                new UserStory
                {
                    Title = "Story1",
                    ImpactedArea = "Integration",
                    Priority = 4,
                    Summary = "Send transaction message to xyz",
                    StoryPointEstimate = 8
                },
                new UserStory
                {
                    Title = "Story2",
                    ImpactedArea = "User interface",
                    Priority = 4,
                    Summary = "Add new text area to allow user to enter abc",
                    StoryPointEstimate = 2
                }
            };
            var project = _projectDao.GetAllProjects().First();
            var numberOfStories = project.Stories.Count;
            stories.ForEach(us => _projectService.AddUserStoryToProject(project, us));
            Assert.AreEqual(numberOfStories + stories.Count, project.Stories.Count);
        }

        [TestMethod]
        public void TestRemoveUserStoryFromProject()
        {
            var story = new UserStory
            {
                Title = "Story1",
                ImpactedArea = "Integration",
                Priority = 4,
                Summary = "Send transaction message to xyz",
                StoryPointEstimate = 8
            };

            var project = _projectDao.GetAllProjects().First();
            _projectService.AddUserStoryToProject(project, story);
            var totalNumberOfStories = project.Stories.Count;
            _projectService.RemoveUserStoryFromProject(project, story);

            Assert.AreEqual(totalNumberOfStories - 1, project.Stories.Count);
        }
        
        [TestMethod]
        public void TestEditUserStory()
        {
            var story = new UserStory
            {
                Title = "Story1",
                ImpactedArea = "Integration",
                Priority = 4,
                Summary = "Send transaction message to xyz",
                StoryPointEstimate = 8
            };

            var expectedStory = story;

            var project = _projectDao.GetAllProjects().First();
            _projectService.AddUserStoryToProject(project, story);

            List<UserStory> userStories = _projectService.GetAllUserStoriesForProject(project).ToList();
            
            story.Summary = "Edited Summary";
            userStories[userStories.Count] = story;

            project.Stories = userStories.ToList();
            _projectService.UpdateProject(project);

            var actualResult = _projectDao.GetAllProjects().First().Stories.ToList()[project.Stories.Count];

            Assert.AreNotEqual(expectedStory, actualResult);
        }
         * */
        //[TestMethod]
        //public void TestAddMemberToProject()
        //{
        //    var memberPass = "Password";
        //    var salt = Encryption.CreateSalt(32);
        //    var saltedValidPass = Encryption.GenerateSha256Hash(memberPass.ToSecureString(), salt);
        //    var member = new User { Username = "ProjectMember1", Password = saltedValidPass, Salt = salt };

        //    var project = _projectDao.GetAllProjects().First();
        //    var totalNumberOfMembers = project.Members.Count;
        //    _projectService.AddMemberToProject(project, member);

        //    Assert.AreEqual(totalNumberOfMembers + 1, project.Members.Count);
        //}

        //[TestMethod]
        //public void TestAddMembersToProject()
        //{
        //    var memberPass = "Password";
        //    var salt = Encryption.CreateSalt(32);
        //    var saltedValidPass = Encryption.GenerateSha256Hash(memberPass.ToSecureString(), salt);

        //    var members = new List<User>
        //    {
        //        new User { Username = "ProjectMember1", Password = saltedValidPass, Salt = salt },
        //        new User { Username = "ProjectMember2", Password = saltedValidPass, Salt = salt }
        //    };

        //    var project = _projectDao.GetAllProjects().First();
        //    var totalNumberOfMembers = project.Members.Count;
        //    _projectService.AddMembersToProject(project, members);

        //    Assert.AreEqual(totalNumberOfMembers + members.Count, project.Members.Count);
        //}

        //[TestMethod]
        //public void TestRemoveMemberFromProject()
        //{
        //    var memberPass = "Password";
        //    var salt = Encryption.CreateSalt(32);
        //    var saltedValidPass = Encryption.GenerateSha256Hash(memberPass.ToSecureString(), salt);
        //    var member = new User { Username = "ProjectMember1", Password = saltedValidPass, Salt = salt };

        //    var project = _projectDao.GetAllProjects().First();
        //    _projectService.AddMemberToProject(project, member);
        //    var totalNumberOfMembers = project.Members.Count;
        //    _projectService.RemoveMemberFromProject(project, member);

        //    Assert.AreEqual(totalNumberOfMembers - 1, project.Members.Count);
        //}

        //[TestMethod]
        //public void TestRemoveMembersFromProject()
        //{
        //    var memberPass = "Password";
        //    var salt = Encryption.CreateSalt(32);
        //    var saltedValidPass = Encryption.GenerateSha256Hash(memberPass.ToSecureString(), salt);

        //    var members = new List<User>
        //    {
        //        new User { Username = "ProjectMember1", Password = saltedValidPass, Salt = salt },
        //        new User { Username = "ProjectMember2", Password = saltedValidPass, Salt = salt }
        //    };

        //    var project = _projectDao.GetAllProjects().First();
        //    _projectService.AddMembersToProject(project, members);
        //    var totalNumberOfMembers = project.Members.Count;
        //    _projectService.RemoveMembersFromProject(project, members);

        //    Assert.AreEqual(totalNumberOfMembers - members.Count, project.Members.Count);
        //}
    }
}