﻿using ScrumManagementServer.Entity;

namespace ScrumManagementTests.Mock_Helpers
{
    class MockScrumContextFactory : IScrumContextFactory
    {
        private readonly ScrumContext _context;

        public MockScrumContextFactory(ScrumContext mockContext)
        {
            _context = mockContext;
        }

        public ScrumContext GetContext()
        {
            return _context;
        }
    }
}
