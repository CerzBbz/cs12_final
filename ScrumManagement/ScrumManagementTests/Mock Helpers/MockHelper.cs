﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementTests.Mock_Helpers
{
    public static class MockHelper
    {
        /// <summary>
        /// Returns a ScrumContext with empty tables as opposed to null tables
        /// </summary>
        /// <returns></returns>
        public static Mock<ScrumContext> GetEmptyContext()
        {
            var context = new Mock<ScrumContext>(string.Empty);
            context.Setup(c => c.Users).Returns(GetMockedDbSet(new List<User>()).Object);
            context.Setup(c => c.UserRoles).Returns(GetMockedUserRoleDbSet().Object);
            context.Setup(c => c.Projects).Returns(GetMockedDbSet(new List<Project>()).Object);
            context.Setup(c => c.UserStories).Returns(GetMockedDbSet(new List<UserStory>()).Object);
            context.Setup(c => c.Tasks).Returns(GetMockedDbSet(new List<Task>()).Object);
            return context;
        }

        /// <summary>
        /// Returns a DbSet for the entity type containing the data provided to mimic the database table
        /// </summary>
        /// <typeparam name="T">An Entity</typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Mock<DbSet<T>> GetMockedDbSet<T>(List<T> data) where T : BaseEntity
        {
            var queryableData = data.AsQueryable();
            var mockSet = new Mock<DbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryableData.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryableData.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryableData.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() =>
            {
                //Prevents data from only being iteratable once
                var enumerator = queryableData.GetEnumerator();
                enumerator.Reset();
                return enumerator;
            });
            mockSet.Setup(m => m.Add(It.IsAny<T>())).Callback<T>(data.Add); //Simulates adding to database
            mockSet.Setup(m => m.Remove(It.IsAny<T>())).Callback<T>(t => data.Remove(t)); //Simulates removing from database
            mockSet.Setup(m => m.Include(It.IsAny<string>())).Returns(mockSet.Object); //Prevent includes causing null pointers
            return mockSet;
        }
        
        /// <summary>
        /// Returns a UserRole DbSet with reference data to mimic the database table
        /// Private as the reference data shouldn't be changed in tests
        /// </summary>
        /// <returns></returns>
        private static Mock<DbSet<UserRole>> GetMockedUserRoleDbSet()
        {
            //Moq doesn't support running seed methods, so set it up manually
            var userRoleData = new List<UserRole>
            {
                new UserRole {Id = 1, RoleDescription = "Product Owner"},
                new UserRole {Id = 2, RoleDescription = "Scrum Master"},
                new UserRole {Id = 3, RoleDescription = "Developer"}
            };
            var userRoleMockSet = GetMockedDbSet(userRoleData);
            return userRoleMockSet;
        }
    }
}
