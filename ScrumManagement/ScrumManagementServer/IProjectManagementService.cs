﻿using System.Collections.Generic;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using System.ServiceModel;
using ScrumManagementServer.DataContracts.DTO_classes;
using System;

namespace ScrumManagementServer
{
    [ServiceContract]
    public interface IProjectManagementService
    {
        [OperationContract]
        ProjectReturnCode CreateProject(string title, int ownerId, int managerId, DateTime startDate, string description);

        [OperationContract]
        void UpdateProject(ProjectDto project);

        [OperationContract]
        List<UserDto> GetAllUsersLazy();

        [OperationContract]
        List<UserDto> GetAllUsers();

        [OperationContract]
        List<UserDto> GetUsersWithRole(string description);

        [OperationContract]
        List<string> GetUsernamesFromSearch(string email, string skill, bool po, bool dev, bool sm);

        [OperationContract]
        void AddMemberToProject(Project project, User memberToAdd);

        [OperationContract]
        void AddUserStoryToProject(ProjectDto project, UserStoryDto story);

        [OperationContract]
        List<UserStoryDto> GetAllUserStoriesForProject(ProjectDto project);

        [OperationContract]
        List<UserDto> GetAllUsersForProjectTemp();

        [OperationContract]
        List<UserDto> GetAllUsersForProject(Project project);

        [OperationContract]
        List<ProjectDto> GetAllProjects();

        [OperationContract]
        List<UserStoryDto> GetAllUserStoriesForProjectID(int id);

        [OperationContract]
        Project GetProjectFromID(int id);

        [OperationContract]
        List<string> GetSkillsFromUsernames(List<string> usernames);

        [OperationContract]
        List<SprintDto> GetSprintsFromProject(ProjectDto project);

		[OperationContract]
        void AddSprintToProject(Project project, Sprint sprint);

        [OperationContract]
        void UpdateUserdetails(User user);

        [OperationContract]
        void SetUserStoryEditableTo(UserStoryDto userStory, bool editable);

        [OperationContract]
        void UpdateUserStory(ProjectDto project, UserStoryDto userStory);
    }
}
