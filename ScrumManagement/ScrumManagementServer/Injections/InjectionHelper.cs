﻿using System;
using Omu.ValueInjecter;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.Injections
{
    //ToDo: Implement caching/expressions to improve performance due to reflection
    public static class InjectionHelper
    {
        public static IDto ConvertToDto(this BaseEntity source)
        {
            var dtoClassEquivalent = GetDtoType(source.GetType());
            var dto = Activator.CreateInstance(dtoClassEquivalent);
            dto.InjectFrom<ScrumInjection>(new InjectableObject { SourceObject = source });
            return dto as IDto;
        }

        public static BaseEntity ConvertToEntity(this IDto source)
        {
            var entityClassEquivalent = GetEntityType(source.GetType());
            var entity = Activator.CreateInstance(entityClassEquivalent);
            entity.InjectFrom<ScrumInjection>(new InjectableObject { SourceObject = source });
            return entity as BaseEntity;
        }

        private static Type GetDtoType(Type entityType)
        {
            if (entityType == typeof(User))
                return typeof (UserDto);
            if (entityType == typeof (UserRole))
                return typeof (UserRoleDto);
            if (entityType == typeof (Project))
                return typeof (ProjectDto);
            if (entityType == typeof (UserStory))
                return typeof (UserStoryDto);
            if (entityType == typeof (Task))
                return typeof (TaskDto);
            if (entityType == typeof (Sprint))
                return typeof (SprintDto);

            throw new Exception(entityType.Name + " doesn't have an equivalent Dto object to map to.");
        }

        private static Type GetEntityType(Type dtoType)
        {
            if (dtoType == typeof(UserDto))
                return typeof(User);
            if (dtoType == typeof(UserRoleDto))
                return typeof(UserRole);
            if (dtoType == typeof(ProjectDto))
                return typeof(Project);
            if (dtoType == typeof(UserStoryDto))
                return typeof(UserStory);
            if (dtoType == typeof(TaskDto))
                return typeof(Task);
            if (dtoType == typeof(SprintDto))
                return typeof(Sprint);

            throw new Exception(dtoType.Name + " doesn't have an equivalent Entity to map to.");
        }
    }
}