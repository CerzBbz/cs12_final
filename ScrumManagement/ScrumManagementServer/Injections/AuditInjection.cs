﻿using System;
using System.Reflection;
using Omu.ValueInjecter.Injections;

namespace ScrumManagementServer.Injections
{
    public class AuditInjection : LoopInjection
    {
        private object _source;

        protected override void Inject(object source, object target)
        {
            _source = source;
            base.Inject(source, target);
        }

        protected override bool MatchTypes(Type source, Type target)
        {
            var defaultMatch = base.MatchTypes(source, target);
            var customMatch = target.Name.Equals(source.Name + "_AUD");
            return defaultMatch || customMatch;
        }

        protected override void Execute(PropertyInfo sp, object source, object target)
        {
            base.Execute(sp, source, target);
        }

        protected override string GetTargetProp(string sourceName)
        {
            if (sourceName.ToLower().Equals("id"))
                return _source.GetType().Name + "Id";
            return base.GetTargetProp(sourceName);
        }
    }
}