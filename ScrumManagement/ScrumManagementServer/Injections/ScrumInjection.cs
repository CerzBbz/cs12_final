﻿using System;
using System.Linq;
using System.Reflection;
using Omu.ValueInjecter;
using Omu.ValueInjecter.Injections;
using System.Collections;
using System.Collections.Generic;
using ScrumManagementServer.Extensions;
using ScrumManagementServer.Domain;
using ScrumManagementServer.DataContracts.DTO_classes;

namespace ScrumManagementServer.Injections
{
    /// <summary>
    /// Deep clones an Entity, including collections
    /// Heavily Modified from examples provided by the developer of ValueInjecter (Omu) and on the github page
    /// ToDo: Implement caching and refactor Dto/Entity logic into merged methods
    /// </summary>
    public class ScrumInjection : LoopInjection
    {
        private static object originalSource, originalTarget;
        private static bool isChild;

        protected override bool MatchTypes(Type source, Type target)
        {
            var defaultMatch = base.MatchTypes(source, target);
            var customMatch = (source.Name + "Dto").Equals(target.Name)
                || (source.Name.Equals(target.Name + "Dto"));
            return defaultMatch || customMatch;
        }

        protected override void Inject(object source, object target)
        {
            //Extract values from the wrapper class
            var injectableSourceObject = source as InjectableObject;
            isChild = injectableSourceObject.IsChildEntity;
            originalSource = injectableSourceObject.OriginalSource;
            originalTarget = injectableSourceObject.OriginalTarget;
            base.Inject(injectableSourceObject.SourceObject, target); // ?? injectableSourceObject.OriginalSource
        }

        protected override void Execute(PropertyInfo sourceProperty, object source, object target)
        {
            var targetProperty = target.GetType().GetProperty(sourceProperty.Name);
            if (targetProperty.IsNull()) return;

            var sourcePropertyValue = sourceProperty.GetValue(source);
            if (sourcePropertyValue.IsNull()) return;
            
            //Try to clone the source property value into the target property
            targetProperty.SetValue(target, GetClone(sourceProperty, targetProperty, sourcePropertyValue, source, target));
        }

        private static object GetClone(PropertyInfo sourceProperty, PropertyInfo targetProperty, object sourceValue, object source, object target)
        {
            if (targetProperty.PropertyType.IsValueType || targetProperty.PropertyType == typeof(string))
            {
                //If its a standard type i.e int, bool... or a string - copy the value, as there's no complex logic
                return sourceValue;
            }

            if(IsPossibleSelfReference(targetProperty)) //Quick check
            {
                //Complete checks
                if(IsSelfReferenceEntity(sourceProperty, targetProperty, source)
                    || IsSelfReferenceDto(sourceProperty, targetProperty, source))
                    return originalTarget; //Return original target reference, which will graduate populate recursively
            }

            //For collections
            if (targetProperty.PropertyType.IsGenericType)
            {
                var genericTargetType = targetProperty.PropertyType.GetGenericArguments()[0];
                var genericSourceType = sourceProperty.PropertyType.GetGenericArguments()[0];

                if (targetProperty.PropertyType.GetGenericTypeDefinition().GetInterfaces().Contains(typeof(IEnumerable)))
                {
                    var listType = typeof(List<>).MakeGenericType(genericTargetType);
                    var list = Activator.CreateInstance(listType);

                    var addMethod = listType.GetMethod("Add");
                    foreach (var item in (IEnumerable)sourceValue)
                    {
                        var listItem = Activator.CreateInstance(genericTargetType);
                        if (genericTargetType.IsValueType || genericTargetType == typeof(string))
                            listItem = item;
                        else
                        {
                            //Get wrapper as this injection will be recursive
                            listItem = Activator.CreateInstance(genericTargetType).InjectFrom<ScrumInjection>(
                                GetInjectableObject(genericSourceType, source, target, item)); 
                        }
                        
                        //As the type of the list is inferred at runtime, we have to use reflection to add to the list
                        addMethod.Invoke(list, new[] { listItem });
                    }

                    return list;
                }

                throw new Exception("CloneInjection : Unhandled generic type " + genericTargetType);
            }

            return Activator.CreateInstance(targetProperty.PropertyType)
                .InjectFrom<ScrumInjection>(GetInjectableObject(sourceProperty, source, target, sourceValue));
        }

        private static InjectableObject GetInjectableObject(Type sourceType, object source, object target, object injectableSource)
        {
            //Store currently available information which will be used within the recursive stack
            var isChildEntity = IsChildEntity(sourceType, source) || IsChildDto(sourceType, source);
            var objectToInjectFrom = new InjectableObject { SourceObject = injectableSource, IsChildEntity = isChildEntity };
            if (isChildEntity)
            {
                objectToInjectFrom.OriginalSource = source;
                objectToInjectFrom.OriginalTarget = target;
            }
            return objectToInjectFrom;
        }

        private static InjectableObject GetInjectableObject(PropertyInfo sourceProperty, object source, object target, object injectableSource)
        {
            return GetInjectableObject(sourceProperty.PropertyType, source, target, injectableSource);
        }

        private static bool IsPossibleSelfReference(PropertyInfo targetProperty)
        {
            return (originalTarget != null && targetProperty.PropertyType == originalTarget.GetType() && isChild) ;
        }

        private static bool IsSelfReferenceEntity(PropertyInfo sourceProperty, PropertyInfo targetProperty, object source)
        {
            if (sourceProperty.PropertyType.BaseType != typeof(BaseEntity) && originalSource.GetType().BaseType != typeof(BaseEntity)) return false;

            var sourcePropertyType = sourceProperty.PropertyType;
            var sourceType = originalSource.GetType();

            if (!sourcePropertyType.Equals(sourceType)) return false;
            if (!(typeof(IDto).IsAssignableFrom(targetProperty.PropertyType))) return false;
            if (!sourceProperty.Name.Equals(targetProperty.Name)) return false;

            var sourcePropertyEntity = sourceProperty.GetValue(source) as BaseEntity;
            var sourceEntity = originalSource as BaseEntity;

            //An Id match means the property currently being cloned is the original entity
            return (sourceEntity.Id.Equals(sourcePropertyEntity.Id));
        }

        private static bool IsSelfReferenceDto(PropertyInfo sourceProperty, PropertyInfo targetProperty, object source)
        {
            if (!(typeof(IDto).IsAssignableFrom(sourceProperty.PropertyType)) && !(typeof(IDto).IsAssignableFrom(originalSource.GetType()))) return false;

            var sourcePropertyType = sourceProperty.PropertyType;
            var sourceType = originalSource.GetType();

            if (!sourcePropertyType.Equals(sourceType)) return false;
            if (targetProperty.PropertyType.BaseType != typeof(BaseEntity)) return false;
            if (!sourceProperty.Name.Equals(targetProperty.Name)) return false;

            var sourcePropertyDto = sourceProperty.GetValue(source) as IDto;
            var sourceDto = originalSource as IDto;

            //An Id match means the property currently being cloned is the original Dto
            return (sourceDto.Id.Equals(sourcePropertyDto.Id));
        }

        private static bool IsChildEntity(Type targetType, object source)
        {
            return (targetType.BaseType == typeof(BaseEntity) && source.GetType().BaseType == typeof(BaseEntity));
        }

        private static bool IsChildDto(Type targetType, object source)
        {
            //If the target and source both implement the IDto interface
            return ((typeof(IDto).IsAssignableFrom(targetType)) && (typeof(IDto).IsAssignableFrom(source.GetType())));
        }
    }
}