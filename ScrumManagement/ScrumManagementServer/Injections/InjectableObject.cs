﻿namespace ScrumManagementServer.Injections
{
    /// <summary>
    /// Wrapper class for tracking self referencing Entities/DTOs during conversions/injections
    /// </summary>
    public class InjectableObject
    {
        public object SourceObject { get; set; }
        public object OriginalSource { get; set; }
        public object OriginalTarget { get; set; }
        public bool IsChildEntity { get; set; }
    }
}