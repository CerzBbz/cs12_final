﻿using ScrumManagementServer.DataContracts.DTO_classes;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ScrumManagementServer
{
    [ServiceContract]
    public interface ITaskManagementService
    {
        [OperationContract]
        void CreateOrUpdateTask(TaskDto task);

        [OperationContract]
        void DeleteTask(TaskDto taskToDelete);

        [OperationContract]
        List<Tuple<DateTime, int>> GetDailyHoursRevisionsForTask(TaskDto task);
    }
}