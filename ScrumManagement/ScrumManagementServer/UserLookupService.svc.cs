﻿using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Enum;
using ScrumManagementServer.Services;
using System.Collections.Generic;

namespace ScrumManagementServer
{
    public class UserLookupService : IUserLookupService
    {
        private readonly UserRoleService _userRoleService = new UserRoleService();
        private readonly UserService _userService = new UserService();

        public List<string> GetUsernamesFromSearch(string email, string skill, bool po, bool dev, bool sm)
        {
            return _userService.GetUsernamesFromSearch(email, skill, po, dev, sm);
        }

    }

}
