﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class BaseDao<T> : IDao<T> where T : BaseEntity
    {
        protected IScrumContextFactory Factory;

        protected BaseDao() { } 

        public BaseDao(IScrumContextFactory scrumContextFactory)
        {
            Factory = scrumContextFactory;
        }

        public virtual void Add(T entity)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public virtual void Attach(T entity)
        {
            DbSet set = Factory.GetContext().Set<T>();
            set.Attach(entity);
        }

        public virtual void Update(T entity)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public virtual void AddOrUpdate(T entity)
        {
            using (var context = Factory.GetContext())
            {
                if (entity.Id > 0)
                {
                    context.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    context.Set<T>().Add(entity);
                }
                context.SaveChanges();
            }
            
        }

        public virtual void Delete(T entity)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(entity).State = EntityState.Deleted;
                context.SaveChanges();
            }
            
        }

        public T FindById(int id)
        {
            DbSet<T> dbSet = Factory.GetContext().Set<T>();
            return dbSet.Find(id);
        }

        public IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                IQueryable<T> dbQuery = Factory.GetContext().Set<T>();

                //Apply eager loading based on properties provided, returns lazy equivalent when there's no params
                dbQuery = navigationProperties.Aggregate(dbQuery, (current, navigationProperty) => current.Include(navigationProperty));

                var list = dbQuery
                    .AsNoTracking()
                    .ToList();
                return list;
            }
            catch (Exception)
            {
                return new List<T>();
            }
        }

        public IEnumerable<T> GetAllLazy()
        {
            try
            {
                IQueryable<T> dbQuery = Factory.GetContext().Set<T>();

                var list = dbQuery
                    .AsNoTracking()
                    .ToList();
                return list;
            }
            catch (Exception)
            {
                return new List<T>();
            }
        }

        public List<T> GetAsList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                IQueryable<T> dbQuery = Factory.GetContext().Set<T>();

                //Apply eager loading
                dbQuery = navigationProperties.Aggregate(dbQuery,
                    (current, navigationProperty) => current.Include(navigationProperty));

                var list = dbQuery
                    .AsNoTracking()
                    .Where(@where)
                    .ToList();
                return list;
            }
            catch (Exception)
            {
                return new List<T>();
            }
        }

        public T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = Factory.GetContext().Set<T>();

            //Apply eager loading
            dbQuery = navigationProperties.Aggregate(dbQuery, (current, navigationProperty) => current.Include(navigationProperty));

            return dbQuery
                    .AsNoTracking()
                    .FirstOrDefault(where); 
        }
    }
}