﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.Dao
{
    public interface IDao<T> where T : BaseEntity
    {
        void Add(T entity);
        void Attach(T entity);
        void Update(T entity);
        void AddOrUpdate(T entity);
        void Delete(T entity);
        T FindById(int id);
        List<T> GetAsList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        IEnumerable<T> GetAllLazy();
    }
}
