﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class TaskDao : BaseDao<Task>
    {
        public TaskDao(IScrumContextFactory scrumcontextFactory) : base(scrumcontextFactory)
        {
        }

        public override void Add(Task task)
        {
            using (var context = Factory.GetContext())
            {
                context.Tasks.Add(task);
                if (task.AssignedTo != null)
                    context.Entry(task.AssignedTo).State = EntityState.Unchanged;                

                context.SaveChanges();
            }
        }

        public override void Update(Task task)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(task).State = EntityState.Modified;
                context.UserStories.Attach(task.AssociatedUserStory);
                if (task.AssignedTo != null)
                    context.Users.Attach(task.AssignedTo); //Optional nullable property
                context.SaveChanges();
            }
        }

        public override void Delete(Task task)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(task).State = EntityState.Deleted;
                if(task.AssociatedUserStory != null)
                    context.UserStories.Attach(task.AssociatedUserStory);
                if (task.AssignedTo != null)
                    context.Users.Attach(task.AssignedTo); //Optional nullable property
                context.SaveChanges();
            }
        }

        public IEnumerable<Task_AUD> GetAuditHistoryForTask(Task task)
        {
            return Factory.GetContext().Task_AUD
                .Where(t_Aud => t_Aud.TaskId == task.Id)
                .ToList();
        }

        //Uses Eager loading
        public IEnumerable<Task> GetAllTasks()
        {
            return Factory.GetContext().Tasks
                .Include(t => t.AssignedTo)
                .Include(t => t.AssociatedUserStory)
                .ToList();
        }
    }
}