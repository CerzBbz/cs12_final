﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ScrumManagementServer.Domain;
using System.Linq;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class UserStoryDao : BaseDao<UserStory>
    {
        public UserStoryDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

        public override void Add(UserStory userStory)
        {
            using (var context = Factory.GetContext())
            {
                foreach (var related in userStory.RelatedStories.Concat(userStory.StoriesRelatedTo))
                {
                    if (related.Id != 0)
                        context.RelatedUserStories.Attach(related);
                }
                context.UserStories.Add(userStory);
                context.SaveChanges();
            }
        }

        public override void Update(UserStory userStory)
        {
            using (var context = Factory.GetContext())
            {
                var existingUserStory = context.UserStories
                    .Include(us => us.AssociatedTasks)
                    .SingleOrDefault(us => us.Id == userStory.Id);

                context.UserStories.Attach(existingUserStory);
                context.Entry(existingUserStory).OriginalValues.SetValues(userStory);

                userStory.AssociatedTasks.ToList().ForEach(t =>
                {
                    var existingTask = existingUserStory.AssociatedTasks.SingleOrDefault(t2 => t2.Id == t.Id);
                    if (existingTask == null) //doesn't exist yet
                    {
                        if (t.AssignedTo != null)
                        {
                            var existingUser = context.Users.AsNoTracking().SingleOrDefault(u => u.Id == t.AssignedTo.Id);
                            t.AssignedTo = null;
                            t.AssignedToId = existingUser.Id;
                        }
                        t.AssociatedUserStoryId = existingUserStory.Id;
                        context.Tasks.Add(t);
                        existingUserStory.AssociatedTasks.Add(t);
                    }
                    else
                    {
                        if (!existingTask.Equals(t))
                        {
                            context.Entry(existingTask).CurrentValues.SetValues(t);
                            context.Entry(existingTask).State = EntityState.Modified;
                        }                        
                    }
                });

                var currentTaskIds = userStory.AssociatedTasks.Select(t => t.Id).ToList();
                var deletedTasks = existingUserStory.AssociatedTasks.Where(t => !currentTaskIds.Contains(t.Id)).ToList();
                deletedTasks.ForEach(t => context.Tasks.Remove(t));

                context.SaveChanges();
            }
        }

        public void SetEditableTo(UserStory userStory, bool editable)
        {
            using (var context = Factory.GetContext())
            {
                var sql = "UPDATE UserStories" +
                          " SET Editable = '" + editable + "'" +
                          " WHERE Id = '" + userStory.Id + "'";
                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                }
                catch (Exception)
                { 

                }

            }

        }

        //Uses eager loading
        public IEnumerable<UserStory> GetAllUserStories()
        {
            return Factory.GetContext().UserStories
                .Include(us => us.AssociatedTasks)
                .Include(us => us.RelatedStories)
                .Include(us => us.StoriesRelatedTo)
                .ToList();
        }
    }
}