﻿using System.Collections.Generic;
using ScrumManagementServer.Domain;
using System.Data.Entity;
using System.Linq;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class RelatedUserStoryDao : BaseDao<RelatedUserStory>
    {
        public RelatedUserStoryDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

        public override void Add(RelatedUserStory story)
        {
            using (var context = Factory.GetContext())
            {
                //Prevent User stories being duplicated by telling Entity they already exist
                context.UserStories.Attach(story.MainStory);
                context.UserStories.Attach(story.RelatedStory);
                context.StoryRelationships.Attach(story.Relationship);
                context.RelatedUserStories.Add(story);
                context.SaveChanges();
            }
        }

        public override void Update(RelatedUserStory story)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(story).State = EntityState.Modified;
                context.UserStories.Attach(story.MainStory);
                context.UserStories.Attach(story.RelatedStory);
                context.StoryRelationships.Attach(story.Relationship);
                context.SaveChanges();
            }
        }

        public override void Delete(RelatedUserStory story)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(story).State = EntityState.Deleted;
                //We don't want to delete the UserStories just because they are no longer related
                context.UserStories.Attach(story.MainStory);
                context.UserStories.Attach(story.RelatedStory);
                context.StoryRelationships.Attach(story.Relationship);
                context.SaveChanges();
            }
        }

        public IEnumerable<RelatedUserStory> GetAllRelatedUserStories()
        {
            return Factory.GetContext().RelatedUserStories.ToList();
        }
    }
}