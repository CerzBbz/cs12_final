﻿using System;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;

namespace ScrumManagementServer.Dao
{
    public class SprintDao : BaseDao<Sprint>
    {
        public SprintDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

        public override void Add(Sprint sprint)
        {
            using (var context = Factory.GetContext())
            {
                context.Users.Attach(sprint.ScrumMaster);
                //foreach (var member in sprint.Developers.Where(m => m.Id != 0))
                //{
                //    context.Users.Attach(member);
                //}
                context.Sprints.Add(sprint);
                context.SaveChanges();
            }
        }



        public override void Update(Sprint sprint)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(sprint).State = EntityState.Modified;
                if (sprint.Id != 0)
                    context.Entry(sprint).State = EntityState.Modified;
                else
                    context.Sprints.Add(sprint);

                context.Users.Attach(sprint.ScrumMaster);
                foreach (var member in sprint.Developers.Where(m => m.Id != 0))
                {
                    context.Users.Attach(member);
                }

                context.Entry(sprint).State = EntityState.Unchanged;
                context.SaveChanges();
            }
        }


        public override void Delete(Sprint sprint)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(sprint).State = EntityState.Deleted;
                context.Entry(sprint.ScrumMaster).State = EntityState.Unchanged;
                foreach (var member in sprint.Developers.Where(m => m.Id != 0))
                {
                    context.Users.Attach(member);
                }
                context.SaveChanges();
            }
        }

        public Sprint FindByIdEager(int Id)
        {
            using (var context = Factory.GetContext())
            {
                return context.Sprints
                    .Include(s => s.Developers)
                    .Include(s => s.ScrumMaster)
                    .Include(s => s.Stories)
                    .Include(s => s.Stories.Select(us => us.AssociatedTasks))
                    .FirstOrDefault(s => s.Id == Id);
            }
        }

        //Uses eager loading
        public IEnumerable<Sprint> GetAllSprints()
        {

            return Factory.GetContext().Sprints
                .Include(s => s.Stories)
                    .Include(s => s.ScrumMaster)
                    .Include(s => s.Developers)
                    .ToList();
            
        }

        /**
        This is a temporary solution
        */
        public int AddUserStoryToSprintTemp(int sprintId, int storyId)
        {
            using (var context = Factory.GetContext())
            {
                var sql = "INSERT INTO SprintBacklog " +
                          "VALUES (" + sprintId + ", " + storyId + ")";
                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                    return 1;
                }
                catch (Exception)
                {
                    return 0;
                }
                
            }
        }

        /**
        This is a temporary solution
        */
        public int RemoveUserStoryFromSprintTemp(int sprintId, int storyId)
        {
            using (var context = Factory.GetContext())
            {
                var sql = "DELETE FROM SprintBacklog " +
                          "WHERE SprintId = '" + sprintId + "'  " +
                          "AND UserStoryId = '" + storyId + "'";
                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                    return 1;
                }
                catch (Exception)
                {
                    return 0;
                }

            }
        }

        public int AddProjectSprint(int projectId, int sprintId)
        {
            using (var context = Factory.GetContext())
            {
                var sql = "INSERT INTO ProjectSprints " +
                          "VALUES (" + projectId + ", " + sprintId + ")";
                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                    return 1;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        internal void AddDeveloper(int sprintId, int devId)
        {
            using (var context = Factory.GetContext())
            {
                var sql = "INSERT INTO Sprint_Members" +
                          "VALUES (" + sprintId + ", " + devId + ")";
                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                }
                catch (Exception)
                {
                }
                
            }
        }
    }
}