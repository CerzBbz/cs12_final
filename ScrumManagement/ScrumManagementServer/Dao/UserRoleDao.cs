﻿using System.Linq;
using ScrumManagementServer.Domain;
using System.Collections.Generic;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class UserRoleDao : BaseDao<UserRole>
    {
        public UserRoleDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

        public UserRole GetUserRoleIfExists(string description)
        {
            var matchingUserRole = from role in GetAll()
                                   where role.RoleDescription == description
                                   select role;
            return matchingUserRole.Any() ? matchingUserRole.First() : new UserRole { RoleDescription = description };
        }

        public IEnumerable<UserRole> GetAllUserRoles()
        {
            return Factory.GetContext().UserRoles.ToList();
        }
    }
}