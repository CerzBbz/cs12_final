﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class ProjectDao : BaseDao<Project>
    {
        public ProjectDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

      public override void Add(Project project)
        {
            using (var context = Factory.GetContext())
            {
                //Assumes any user id's have been correctly applied before this point
                context.Projects.Add(project);
                context.SaveChanges();
            }
        }

        public override void Update(Project project)
        {
            using (var context = Factory.GetContext())
            {
                var existingProject = context.Projects
                    .Include(p => p.Stories)
                    .SingleOrDefault(p => p.Id == project.Id);


                context.Projects.Attach(existingProject);
                context.Entry(existingProject).CurrentValues.SetValues(project);
                                
                project.Stories.ToList().ForEach(us =>
                {
                    var existingUserStory = context.UserStories
                                                .SingleOrDefault(eus => eus.Id == us.Id);
                    if(existingUserStory == null)
                    {
                        context.UserStories.Add(us);
                        existingProject.Stories.Add(us);
                    }
                    else if (!existingUserStory.Equals(us))
                    {
                        context.Entry(existingUserStory).CurrentValues.SetValues(us);
                        context.Entry(existingUserStory).State = EntityState.Modified;
                    }                    
                });

                var currentStoryIds = project.Stories.Select(us => us.Id).ToList();
                var deletedStories = existingProject.Stories.Where(us => !currentStoryIds.Contains(us.Id)).ToList();
                deletedStories.ForEach(us => context.UserStories.Remove(us));
                
                context.SaveChanges();
            }
        }

        public override void Delete(Project project)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(project).State = EntityState.Deleted;
                context.Entry(project.Manager).State = EntityState.Unchanged;
                context.Entry(project.Owner).State = EntityState.Unchanged;
                
                context.SaveChanges();
            }
        }

        //Uses eager loading
        public IEnumerable<Project> GetAllProjects()
        {
            return Factory.GetContext().Projects
                .Include(p => p.Manager)
                .Include(p => p.Owner)
                .Include(p => p.Stories)
                .Include(p => p.Sprints)
                .ToList();        
        }

        public IEnumerable<Sprint> GetSprintsFromIds(List<int> sprintIds)
        {
            using (var context = Factory.GetContext())
            {
                return context.Sprints
                    .Where(s => sprintIds.Contains(s.Id))
                    .Include(s => s.ScrumMaster)
                    .Include(s => s.Developers)
                    .Include(s => s.Stories)
                    .ToList();
            }
        }
    }
}