﻿using System.Collections.Generic;
using ScrumManagementServer.Domain;
using System.Data.Entity;
using System.Linq;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Dao
{
    public class UserDao : BaseDao<User>
    {
        public UserDao(IScrumContextFactory scrumContextFactory) : base(scrumContextFactory)
        {
        }

        public override void Add(User user)
        {
            using (var context = Factory.GetContext())
            {                
                context.Users.Add(user);
                user.UserRoles.ToList().ForEach(ur => context.Entry(ur).State = EntityState.Unchanged);
                context.SaveChanges();
            }
        }

        public override void Update(User user)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(user).State = EntityState.Modified;
                user.UserRoles.ToList().ForEach(ur => context.Entry(ur).State = EntityState.Unchanged);
                context.SaveChanges();
            }
        }

        public override void Delete(User user)
        {
            using (var context = Factory.GetContext())
            {
                context.Entry(user).State = EntityState.Deleted;
                foreach (var role in user.UserRoles.Where(role => role.Id != 0))
                {
                    context.UserRoles.Attach(role);
                }
                context.SaveChanges();
            }
        }

        public int GetIdFromName(string username)
        {
            var matchingUser = GetAllUsers().FirstOrDefault(u => u.Username.Equals(username));
            if (matchingUser != null)
                return matchingUser.Id;
            throw new System.Exception("Could not find users ID from username");
        }

        //Uses eager loading
        public IEnumerable<User> GetAllUsers()
        {
            return Factory.GetContext().Users
                .Include(u => u.UserRoles)
                .Include(u => u.AssignedTasks)
                .ToList();
        }

        public IEnumerable<User> GetAllLazy()
        {
            return Factory.GetContext().Users.ToList();
        }
    }
}