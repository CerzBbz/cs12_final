﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManagementServer.Enum
{
    public enum UserRoleEnum
    {
        ScrumMaster,
        Developer,
        ProductOwner
    };
}