﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace ScrumManagementServer.Encryption
{
    public static class Encryption
    {

        //Create the salt allowing passwords to be stored using hash and salt
        public static String CreateSalt(int size)
        {
            //size should generally be 32, until recommended standard changes
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        //Create the hash using input and the generated salt
        public static String GenerateSha256Hash(SecureString input, String salt)
        {
            String s = input.GetString();
            //Combine input and salt and converts them to bytes, makes sure Strings are in utf8 format
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input.GetString() + salt);
            //dispose of password
            input.Dispose();

            System.Security.Cryptography.SHA256Managed sha256HashString =
                new System.Security.Cryptography.SHA256Managed();
            byte[] hash = sha256HashString.ComputeHash(bytes);

            return ByteArrayToHexString(hash);
        }

        //converts a given byte array to a hex string, for nicer looking strings
        private static string ByteArrayToHexString(byte[] bytes)
        {
            string hexString = BitConverter.ToString(bytes);
            return hexString.Replace("-", "");
        }

        public static SecureString ToSecureString(this String source)
        {
            /*
             * method to convert a string to a SecureString
             */
            SecureString result = new SecureString();
            //populate SecureString object appropriatly (char at a time)
            foreach (char c in source)
            {
                result.AppendChar(c);
            }
            //make SecureString readonly
            result.MakeReadOnly();
            return result;
        }

        public static string GetString(this SecureString ssSource)
        {
            /*
             * method to convert a SecureString to a string
             */

            //if source is null don't accept and throw an exception
            if (ssSource == null)
            {
                throw new ArgumentNullException();
            }

            //set up variables used to convert secure string
            int length = ssSource.Length;
            string result = null;
            char[] arrChars = new char[length];
            //pointer to point to where ssSource is in unmanaged memory, set it to zero
            IntPtr pointer = IntPtr.Zero;

            try
            {
                //sets the pointer to where ssSource is in memory
                pointer = Marshal.SecureStringToBSTR(ssSource);

                //copy from where pointer points into arrChars and copy from 0 - ssSource.length
                Marshal.Copy(pointer, arrChars, 0, length);

                //convert char array back to string
                result = String.Join(String.Empty, arrChars);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("SecureString could not be converted to String", e);
            }
            finally
            {
                //if pointer is not zeroed out, zero it out
                if (pointer != IntPtr.Zero)
                {
                    //zeroes out pointer
                    Marshal.ZeroFreeBSTR(pointer);
                }
            }
            return result;
        }
    }
}

