﻿using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Services;
using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Entity;
using System;
using ScrumManagementServer.Injections;

namespace ScrumManagementServer
{
    public class ProjectManagementService : IProjectManagementService
    {
        private readonly ScrumContextFactory _contextFactory = new ScrumContextFactory();
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        private readonly UserStoryService _userStoryService;

        public ProjectManagementService()
        {
            _projectService = new ProjectService(new ProjectDao(_contextFactory));
            _userService = new UserService(new UserDao(_contextFactory));
            _userStoryService = new UserStoryService(new UserStoryDao(_contextFactory), new RelatedUserStoryDao(_contextFactory));
        } 

        public ProjectManagementService(ProjectService projectService, UserService userService, SprintService sprintService, UserStoryService userStoryService)
        {
            _projectService = projectService ?? new ProjectService();
            _userService = userService ?? new UserService();
            _userStoryService = userStoryService ?? new UserStoryService(null,null);
        }

        public ProjectReturnCode CreateProject(string title, int ownerId, int managerId, DateTime startDate, string description)
        {
            var createdProject = new Project
            {
                Title = title,
                StartDate = startDate,
                Description = description,
                ManagerId = managerId //Should have a value as manager is required
            };

            //EF will load these from the Ids, full user objects aren't required
            if (ownerId > 0)
                createdProject.OwnerId = ownerId;
            if (managerId > 0)
                createdProject.ManagerId= managerId;

            _projectService.CreateProject(createdProject);
            return ProjectReturnCode.SuccessfulProjectCreation;
        }

        public void UpdateProject(ProjectDto project)
        {
            _projectService.UpdateProject(project.ConvertToEntity() as Project);
        }

        public List<string> GetUsernamesFromSearch(string email, string skill, bool po, bool dev, bool sm)
        {
            return _userService.GetUsernamesFromSearch(email, skill, po, dev, sm);
        }

        public void AddSprintToProject(Project project, Sprint sprintToAdd)
        {
            _projectService.AddSprintToProject(project, sprintToAdd);
        }

        public void AddMemberToProject(Project project, User memberToAdd)
        {
            throw new NotImplementedException();
        }

        public List<UserDto> GetUsersWithRole(string description)
        {
            return _userService.GetUsersWithRole(description).Select(u => u.ConvertToDto() as UserDto).ToList();
        }

        public List<UserDto> GetAllUsersLazy()
        {
            return _userService.GetAllLazy().Select(u => u.ConvertToDto() as UserDto).ToList();
        }

        public List<ProjectDto> GetAllProjectsLazy()
        {
            return _projectService.GetAllLazy().Select(p => p.ConvertToDto() as ProjectDto).ToList();
        }

        public List<UserDto> GetAllUsers()
        {
            return _userService.GetAllUsers().Select(u => u.ConvertToDto() as UserDto).ToList();
        } 

        public void AddUserStoryToProject(ProjectDto project, UserStoryDto story)
        {
            _projectService.AddUserStoryToProject(project.ConvertToEntity() as Project, story.ConvertToEntity() as UserStory);   
        }

        public List<UserDto> GetAllUsersForProject(Project project)
        {
            throw new NotImplementedException();
        }

        public List<ProjectDto> GetAllProjects()
        {
            return _projectService.GetAllProjects().Select(project => project.ConvertToDto() as ProjectDto).ToList();
        } 

        public List<UserStoryDto> GetAllUserStoriesForProject(ProjectDto project)
        {
            return _projectService.GetAllUserStoriesForProject(project.ConvertToEntity() as Project).Select(us => us.ConvertToDto() as UserStoryDto).ToList();
        }

        public List<UserStoryDto> GetAllUserStoriesForProjectID(int id)
        {
            return _projectService.GetAllUserStoriesForProjectId(id).Select(us => us.ConvertToDto() as UserStoryDto).ToList();
        }

        public Project GetProjectFromID(int id)
        {
            return _projectService.GetProjectFromId(id);
        }

        public List<UserDto> GetAllUsersForProjectTemp()
        {
            return _userService.GetAllUsers().Select(u => u.ConvertToDto() as UserDto).ToList();
        }

        public List<string> GetSkillsFromUsernames(List<string> usernames)
        {
            return _userService.GetSkillsFromUsernames(usernames);
        }

        public List<SprintDto> GetSprintsFromProject(ProjectDto project)
        {
            return
                _projectService.GetSprintsFromProject(project.ConvertToEntity() as Project)
                    .Select(s => s.ConvertToDto() as SprintDto)
                    .ToList();
        }

        public void UpdateUserdetails(User user)
        {
            _userService.UpdateUserDetails(user);
        }

        public void SetUserStoryEditableTo(UserStoryDto userStory, bool editable)
        {
            _userStoryService.SetEditableTo(userStory.ConvertToEntity() as UserStory, editable);
        }

        public void UpdateUserStory(ProjectDto project, UserStoryDto userStory)
        {
            _projectService.UpdateUserStory(project.ConvertToEntity() as Project, userStory.ConvertToEntity() as UserStory);
        }
    }
}
