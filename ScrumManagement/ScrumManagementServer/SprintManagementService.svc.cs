﻿using ScrumManagementServer.Dao;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.Injections;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer
{
    public class SprintManagementService : ISprintManagementService
    {
        private ScrumContextFactory _factory = new ScrumContextFactory();
        private readonly SprintService _sprintService;
        private readonly UserService _userService;        
        private readonly ProjectService _projectService;
        private readonly TaskService _taskService;
        private readonly UserStoryService _userStoryService;

        public SprintManagementService()
        {
            _sprintService = new SprintService(new SprintDao(_factory));
            _userService = new UserService(new UserDao(_factory));            
            _projectService = new ProjectService(new ProjectDao(_factory));
            _taskService = new TaskService(new TaskDao(_factory));
            _userStoryService = new UserStoryService(new UserStoryDao(_factory));
        }

        public SprintManagementService(SprintService sprintService, UserService userService, ProjectService projectService, TaskService taskService)
        {
            _sprintService = sprintService ?? new SprintService();
            _userService = userService ?? new UserService();
            _projectService = projectService ?? new ProjectService();
            _taskService = taskService ?? new TaskService();
        }

        public void CreateSprint(ProjectDto project, string title, int scrumMasterID, DateTime startDate, DateTime endDate, List<UserDto> developers)
        {
            var createdSprint = new Sprint()
            {
                Title = title,
                ScrumMaster = _userService.GetAllUsers().FirstOrDefault(u => u.Id.Equals(scrumMasterID)),
                StartDate = startDate,
                EndDate = endDate,
                Developers = developers.Select(u => u.ConvertToEntity() as User).ToList()
            };
           
            _sprintService.CreateSprint(project.ConvertToEntity() as Project, createdSprint);
        }

        public UserDto GetScrumMasterFromSprint(int sprintId)
        {
            return _sprintService.GetScrumMasterFromSprint(sprintId).ConvertToDto() as UserDto;
        }

        public List<UserStoryDto> GetUserStoriesFromSprintId(int id)
        {
            return _sprintService.GetUserStoriesFromSprintId(id).Select(us => us.ConvertToDto() as UserStoryDto).ToList();
        }

        public List<UserStoryDto> GetUserStoriesFromSprint(SprintDto sprint)
        {
            return _sprintService.GetUserStoriesFromSprint(sprint.ConvertToEntity() as Sprint).Select(us => us.ConvertToDto() as UserStoryDto).ToList();
        }

        public int AddUserStoryToSprint(SprintDto sprint, UserStoryDto story)
        {
            return _sprintService.AddUserStoryToSprint(sprint.ConvertToEntity() as Sprint, story.ConvertToEntity() as UserStory);
        }

        public int RemoveUserStoryFromSprint(SprintDto sprint, UserStoryDto story)
        {
            return _sprintService.RemoveUserStoryFromSprint(sprint.ConvertToEntity() as Sprint, story.ConvertToEntity() as UserStory);
        }

        public void UpdateUserStory(UserStoryDto userStory)
        {
            _userStoryService.UpdateUserStory(userStory.ConvertToEntity() as UserStory);
        }

        public int GetTotalEstimatedHoursOnTasksForSprint(SprintDto sprint)
        {
            var existingSprint = _sprintService.GetSprintFromId(sprint.Id);
            return existingSprint != null ?
                existingSprint.Stories.Sum(us => us.AssociatedTasks.Sum(t => t.EstimatedHours)) : 0;
        }

        public List<List<Tuple<DateTime, int>>> GetRemainingHoursOnTasksFromSprintDaily(SprintDto sprint)
        {
            var existingSprint = _sprintService.GetSprintFromId(sprint.Id);
            var allDailyRevisionsForAllTasksInSprint = new List<List<Tuple<DateTime, int>>>();
            existingSprint.Stories.ToList().ForEach(us =>
            {
                us.AssociatedTasks.ToList().ForEach(t =>
                        allDailyRevisionsForAllTasksInSprint.Add(_taskService.GetDailyRevisionsForTask(t).ToList()));
            });
            return allDailyRevisionsForAllTasksInSprint;
        }

        public List<SprintDto> GetAllSprints()
        {
            return _sprintService.GetAllSprints().Select(sprint => sprint.ConvertToDto() as SprintDto).ToList();
        }

    }
}
