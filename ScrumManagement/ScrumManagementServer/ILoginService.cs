﻿using System.Collections.Generic;
using System.ServiceModel;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer
{
    [ServiceContract]
    public interface ILoginService
    {
        [OperationContract]
        LoginReturnCode RegisterNewUser(string userName, string password, string salt, string skills, List<UserRole> roles = null );

        [OperationContract]
        LoginReturnCode AuthenticateUserDetails(string userName, string password, string salt);

        [OperationContract]
        bool IsUserNameValid(string userName);

        [OperationContract]
        string GetSalt(User user);
    }
}
