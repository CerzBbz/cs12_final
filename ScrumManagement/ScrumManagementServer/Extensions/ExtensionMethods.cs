﻿using System;

namespace ScrumManagementServer.Extensions
{
    public static class ExtensionMethods
    {
        public static bool IsNull<T>(this T source) where T : class
        {
            return source == null;
        }

        //For nullable properties
        public static bool IsNull<T>(this T? source) where T : struct
        {
            return !source.HasValue;
        }

        public static bool IsNotNull<T>(this T source) where T : class
        {
            return source != null;
        }

        //For nullable properties
        public static bool IsNotNull<T>(this T? source) where T : struct
        {
            return source.HasValue;
        }

        public static string ReplaceLast(this string source, string targetText)
        {
            var lastIndex = source.LastIndexOf(targetText, StringComparison.Ordinal);
            return lastIndex == -1 ? source : source.Substring(0, lastIndex-1);
        }
    }
}