﻿using ScrumManagementServer.Entity;
using System;

namespace ScrumManagementServer.Domain
{
    public class Task_AUD : BaseEntity, IAuditEntity
    {
        public int TaskId { get; set; } //The task being audited
        public string Title { get; set; }
        public string GeneralPriority { get; set; }
        public string ImpactedArea { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public int EstimatedHours { get; set; }
        public int RemainingHours { get; set; }
        public int CompletedHours { get; set; }

        public int AssignedToId { get; set; }
        public int AssociatedUserStoryId { get; set; }

        //Audit properties
        public AuditType AuditType { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}