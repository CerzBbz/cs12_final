﻿namespace ScrumManagementServer.Domain
{
    public class RelatedUserStory : BaseEntity
    {
        public int MainStoryId { get; set; }
        public int RelatedId { get; set; }
        public int RelationshipId { get; set; }

        public virtual UserStory MainStory { get; set; }       
        public virtual UserStory RelatedStory { get; set; }
        public virtual StoryRelationship Relationship { get; set; }
    }
}