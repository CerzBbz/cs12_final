﻿using System;
using System.Collections.Generic;

namespace ScrumManagementServer.Domain
{
    public class Project : BaseEntity
    {
        public Project()
        {
            Sprints = new List<Sprint>();
            Stories = new List<UserStory>();
        }

        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public string Description { get; set; }

        public int? OwnerId { get; set; }
        public User Owner { get; set; }

        public int ManagerId { get; set; }
        public User Manager { get; set; }

        public ICollection<UserStory> Stories { get; set; }
        public ICollection<Sprint> Sprints { get; set; }
    }
}