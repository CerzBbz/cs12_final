﻿namespace ScrumManagementServer.Domain
{
    public class UserRole : BaseEntity
    {
        public string RoleDescription { get; set; }
    }
}