﻿using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Domain
{
    [Audited]
    public class Task : BaseEntity
    {
        public string Title { get; set; }
        public string GeneralPriority { get; set; }
        public string ImpactedArea { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public int EstimatedHours { get; set; }
        public int RemainingHours { get; set; }
        public int CompletedHours { get; set; }

        public int? AssignedToId { get; set; }
        public User AssignedTo { get; set; }
        public int AssociatedUserStoryId { get; set; }        
        public UserStory AssociatedUserStory { get; set; }

        public bool Equals(Task otherTask)
        {
            return Id == otherTask.Id 
                && AssignedToId == otherTask.AssignedToId
                && AssociatedUserStoryId == otherTask.AssociatedUserStoryId
                && Title.Equals(otherTask.Title)
                && GeneralPriority.Equals(otherTask.GeneralPriority)
                && ImpactedArea.Equals(otherTask.ImpactedArea)
                && Status.Equals(otherTask.Status)
                && StatusReason.Equals(otherTask.StatusReason)
                && EstimatedHours == otherTask.EstimatedHours
                && RemainingHours == otherTask.RemainingHours
                && CompletedHours == otherTask.CompletedHours;
        }
    }
}