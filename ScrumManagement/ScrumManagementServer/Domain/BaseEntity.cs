﻿namespace ScrumManagementServer.Domain
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}