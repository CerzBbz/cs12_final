﻿using System.Collections.Generic;
using System.Linq;

namespace ScrumManagementServer.Domain
{
    public class User : BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string Skills { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<Task> AssignedTasks { get; set; } 

        public User()
        {
            UserRoles = new List<UserRole>();
            AssignedTasks = new List<Task>();
        }

        public bool IsProductOwner()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Product Owner"));
        }

        public bool IsScrumMaster()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Scrum Master"));
        }

        public bool IsDeveloper()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Developer"));
        }
    }
}