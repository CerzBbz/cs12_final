﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManagementServer.Domain
{
    public class Sprint : BaseEntity
    {

        public Sprint()
        {
            Developers = new List<User>();
            Stories = new List<UserStory>();
        }

        public string Title { get; set; }
        public User ScrumMaster { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<User> Developers { get; set; }
        public ICollection<UserStory> Stories { get; set; }
    }
}