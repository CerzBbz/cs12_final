﻿using System.Collections.Generic;

namespace ScrumManagementServer.Domain
{
    public class UserStory : BaseEntity
    {
        public UserStory()
        {
            RelatedStories = new List<RelatedUserStory>();
            StoriesRelatedTo = new List<RelatedUserStory>();
            AssociatedTasks = new List<Task>();
        }

        public string Title { get; set; }
        public string Summary { get; set; }
        public int Priority { get; set; }
        public string ImpactedArea { get; set; }
        public int StoryPointEstimate { get; set; }
        public bool Editable { get; set; }

        public ICollection<RelatedUserStory> RelatedStories { get; set; }
        public ICollection<RelatedUserStory> StoriesRelatedTo { get; set; }
        public ICollection<Task> AssociatedTasks { get; set; } 

        public bool Equals(UserStory otherStory)
        {
            return Id == otherStory.Id
                && Title.Equals(otherStory.Title)
                && Summary.Equals(otherStory.Summary)
                && Priority == otherStory.Priority
                && ImpactedArea.Equals(otherStory.ImpactedArea)
                && StoryPointEstimate == otherStory.StoryPointEstimate;
        }
    }
}