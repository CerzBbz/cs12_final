﻿namespace ScrumManagementServer.Domain
{
    public class StoryRelationship : BaseEntity
    {
        public StoryRelationship()
        {

        }

        public string Description { get; set; }
    }
}