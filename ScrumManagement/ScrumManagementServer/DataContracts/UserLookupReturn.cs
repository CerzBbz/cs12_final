﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ScrumManagementServer.DataContracts
{
    [DataContract(Name = "UserLookupReturn")]
    public class UserLookupReturn
    {
        [DataMember]
        private List<string> _retList;
        [DataMember]
        public ResponseCode Code;

        public List<string> ReturnedList
        {
            get { return _retList; }
            set { _retList = value; }

        }

        [DataContract]
        public enum ResponseCode
        {
            Empty,
            Error,
            Success
        }



    }
}