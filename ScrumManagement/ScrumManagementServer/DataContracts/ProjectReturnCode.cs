﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ScrumManagementServer.DataContracts
{
    [DataContract(Name = "ProjectReturnCode")]
    public enum ProjectReturnCode
    {
        [EnumMember]
        InvalidProjectName = 1,
        [EnumMember]
        InvalidOwnerSelected = 2,
        [EnumMember]
        InvalidManagerSelected = 3,
        [EnumMember]
        SuccessfulProjectCreation = 4,

    }

}