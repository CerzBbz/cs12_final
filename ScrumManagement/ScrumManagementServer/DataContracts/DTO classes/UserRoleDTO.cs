﻿using System.Runtime.Serialization;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract(IsReference = true)]
    public class UserRoleDto : IDto
    {
        public UserRoleDto() { }

        [DataMember]
        public int Id { get; private set; }

        [DataMember]
        public string RoleDescription {get; set;}
    }
}