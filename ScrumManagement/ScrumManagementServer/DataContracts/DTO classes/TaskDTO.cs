﻿using ScrumManagementServer.Domain;
using System.Runtime.Serialization;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract(IsReference = true)]
    public class TaskDto : IDto
    {
        //To allow the client to create new instances and pass to the server for creation etc
        public TaskDto()
        {
            //As this will be created from the client it will not exist in the database.
            //An Id of 0 will cause the DAL to create it
            Id = 0;
        }

        [DataMember]
        public int Id { get; private set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string GeneralPriority { get; set; }
        [DataMember]
        public string ImpactedArea { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string StatusReason { get; set; }
        [DataMember]
        public int EstimatedHours { get; set; }
        [DataMember]
        public int RemainingHours { get; set; }
        [DataMember]
        public int CompletedHours { get; set; }

        [DataMember]
        public int? AssignedToId { get; set; }
        [DataMember]
        public UserDto AssignedTo { get; set; }
        [DataMember]
        public int AssociatedUserStoryId { get; set; }
    }
}