﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ScrumManagementServer.Domain;
using System;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract(IsReference =true)]
    public class ProjectDto : IDto
    {
        //To allow the client to create new instances and pass to the server for creation etc
        public ProjectDto()
        {
            //As this will be created from the client it will not exist in the database.
            //An Id of 0 will cause the DAL to create it
            Id = 0;
        }

        [DataMember]
        public int Id { get; private set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int ManagerId { get; set; }

        [DataMember]
        public UserDto Manager { get; set; }

        [DataMember]
        public int? OwnerId { get; set; }

        [DataMember]
        public UserDto Owner { get; set; }

        [DataMember]
        public ICollection<UserStoryDto> Stories { get; set; }

        [DataMember]
        public ICollection<UserDto> Members { get; set; }

        [DataMember]
        public ICollection<SprintDto> Sprints { get; set; } 
    }
}