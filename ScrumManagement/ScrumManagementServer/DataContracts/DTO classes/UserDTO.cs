﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract(IsReference = true)]
    public class UserDto : IDto
    {
        //To allow the client to create new instances and pass to the server for creation etc
        public UserDto()
        {
            //As this will be created from the client it will not exist in the database.
            //An Id of 0 will cause the DAL to create it
            Id = 0;
        }

        [DataMember]
        public int Id { get; private set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public string Skills { get; set; }

        [DataMember]
        public ICollection<UserRoleDto> UserRoles { get; set; }

        [DataMember]
        public ICollection<TaskDto> AssignedTasks { get; set; } 

        public bool IsProductOwner()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Product Owner"));
        }

        public bool IsScrumMaster()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Scrum Master"));
        }

        public bool IsDeveloper()
        {
            return UserRoles.Any(ur => ur.RoleDescription.Equals("Developer"));
        }
    }
}