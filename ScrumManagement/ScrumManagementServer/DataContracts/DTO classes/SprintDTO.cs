﻿using ScrumManagementServer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ScrumManagementServer.DataContracts.DTO_classes;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract]
    public class SprintDto : IDto
    {
        //To allow the client to create new instances and pass to the server for creation etc
        public SprintDto()
        {
            //As this will be created from the client it will not exist in the database.
            //An Id of 0 will cause the DAL to create it
            Id = 0;
        }

        [DataMember]
        public int Id { get; private set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public UserDto ScrumMaster { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public List<UserDto> Developers { get; set; }

        [DataMember]
        public List<UserStoryDto> Stories { get; set; } 
    }
}