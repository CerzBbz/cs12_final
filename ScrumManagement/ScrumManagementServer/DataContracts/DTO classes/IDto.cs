﻿namespace ScrumManagementServer.DataContracts.DTO_classes
{
    public interface IDto
    {
        //Marker interface
        int Id { get; }
    }
}
