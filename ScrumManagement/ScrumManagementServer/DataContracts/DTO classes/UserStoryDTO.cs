﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.DataContracts.DTO_classes
{
    [DataContract(IsReference = true)]
    public class UserStoryDto : IDto
    {
        //To allow the client to create new instances and pass to the server for creation etc
        public UserStoryDto()
        {
            //As this will be created from the client it will not exist in the database.
            //An Id of 0 will cause the DAL to create it
            Id = 0;
        }

        [DataMember]
        public int Id { get; private set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Summary { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string ImpactedArea { get; set; }

        [DataMember]
        public int StoryPointEstimate { get; set; }

        [DataMember]
        public List<TaskDto> AssociatedTasks { get; set; } 

        [DataMember]
        public bool Editable { get; set; }
    }
}