﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ScrumManagementServer.DataContracts
{
    [DataContract(Name = "LoginReturnCode")]
    public enum LoginReturnCode
    {
        [EnumMember]
        InvalidEmailAddress = 1,
        [EnumMember]
        InvalidPasswordComplexity = 2,
        [EnumMember]
        UsernameAlreadyExists = 3,
        [EnumMember]
        SuccessfulRegistration = 4,
        [EnumMember]
        InvalidLoginCredentials = 5,
        [EnumMember]
        SuccessfulLogin = 6,
        [EnumMember]
        ValidUsername = 7,
    }
}