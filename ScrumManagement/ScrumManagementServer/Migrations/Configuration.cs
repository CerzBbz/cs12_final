using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Migrations
{
    using System.Data.Entity.Migrations;
    using Domain;

    internal sealed class Configuration : DbMigrationsConfiguration<ScrumContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ScrumContext context)
        {
            
            //Reset identity count
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('UserRoles', RESEED, 0);");
            context.UserRoles.AddOrUpdate(
                ur => ur.RoleDescription,
                    new UserRole { Id = 1, RoleDescription = "Product Owner" },
                    new UserRole { Id = 2, RoleDescription = "Scrum Master" },
                    new UserRole { Id = 3, RoleDescription = "Developer" }
                );

            //Reset identity count
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('StoryRelationships', RESEED, 0);");
            context.StoryRelationships.AddOrUpdate(
                sr => sr.Description,
                    new StoryRelationship { Id = 1, Description = "Related"},
                    new StoryRelationship { Id = 2, Description = "Depends on"},
                    new StoryRelationship { Id = 3, Description = "Duplicate"}
                );
               
        }
    }
}
