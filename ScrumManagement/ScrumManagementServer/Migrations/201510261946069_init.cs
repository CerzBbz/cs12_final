namespace ScrumManagementServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User_UserRole",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        UserRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.UserRoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.UserRoles", t => t.UserRoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.UserRoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User_UserRole", "UserRoleId", "dbo.UserRoles");
            DropForeignKey("dbo.User_UserRole", "UserId", "dbo.Users");
            DropIndex("dbo.User_UserRole", new[] { "UserRoleId" });
            DropIndex("dbo.User_UserRole", new[] { "UserId" });
            DropTable("dbo.User_UserRole");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
        }
    }
}
