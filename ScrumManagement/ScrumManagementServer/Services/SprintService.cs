﻿using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;
using System.Collections.Generic;
using System.Linq;


namespace ScrumManagementServer.Services
{
    public class SprintService
    {

        private readonly SprintDao _sprintDao;

        public SprintService(SprintDao dao = null)
        {
            _sprintDao = dao ?? new SprintDao(new ScrumContextFactory());
        }

        public void CreateSprint(Project project, Sprint sprint)
        {
            _sprintDao.Add(sprint);
            if (project != null)
            {
                _sprintDao.AddProjectSprint(project.Id, sprint.Id);
                foreach(User d in sprint.Developers)
                {
                    _sprintDao.AddDeveloper(sprint.Id, d.Id);
                }
            }
              

        }

        public IEnumerable<Sprint> GetAllSprints()
        {
            return _sprintDao.GetAllSprints();
        }

        public void AddMemberToSprint(Sprint sprint, User member)
        {

            foreach (var item in member.UserRoles)
            {
                if (item.RoleDescription.Equals("Developer"))
                {
                    sprint.Developers.Add(member);
                }
            }   
            _sprintDao.Update(sprint);
        }


        public User GetScrumMasterFromSprint(int sprintId)
        {
            Sprint sprint = GetSprintFromId(sprintId);
            return sprint.ScrumMaster;
        }

        public List<UserStory> GetUserStoriesFromSprintId(int id)
        {
            Sprint sprint = GetSprintFromId(id);
            /*var story1 = new UserStory //temp
            {
                Title = "Story1",
                ImpactedArea = "Integration",
                Priority = 4,
                Summary = "Send transaction message to xyz",
                StoryPointEstimate = 8
            };
            var story2 = new UserStory //temp
            {
                Title = "Story2",
                ImpactedArea = "Integration",
                Priority = 4,
                Summary = "Send transaction message to xyz",
                StoryPointEstimate = 8
            };
            AddUserStoryToSprint(sprint, story1);
            AddUserStoryToSprint(sprint, story2);*/
            return sprint.Stories.ToList();
        }

        public Sprint GetSprintFromId(int id)
        {
            return _sprintDao.FindByIdEager(id);
        }

        public int AddUserStoryToSprint(Sprint sprint, UserStory story)
        {
             return _sprintDao.AddUserStoryToSprintTemp(sprint.Id, story.Id);
        }

        public int RemoveUserStoryFromSprint(Sprint sprint, UserStory story)
        {
            return _sprintDao.RemoveUserStoryFromSprintTemp(sprint.Id, story.Id);
        }

        public List<UserStory> GetUserStoriesFromSprint(Sprint sprint)
        {
            return sprint.Stories.ToList();
        }
    }
}