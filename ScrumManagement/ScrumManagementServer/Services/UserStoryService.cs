﻿using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Services
{
    public class UserStoryService
    {
        private readonly UserStoryDao _userStoryDao;
        private readonly RelatedUserStoryDao _relatedUserStoryDao;

        public UserStoryService(UserStoryDao userStoryDao, RelatedUserStoryDao relatedDao)
        {
            _userStoryDao = userStoryDao ?? new UserStoryDao(new ScrumContextFactory());
            _relatedUserStoryDao = relatedDao ?? new RelatedUserStoryDao(new ScrumContextFactory());
        }

        public UserStoryService(UserStoryDao userStoryDao)
        {
            _userStoryDao = userStoryDao ?? new UserStoryDao(new ScrumContextFactory());
        }

        public void CreateUserStory(UserStory userStory)
        {
            _userStoryDao.Add(userStory);
        }

        public void UpdateUserStory(UserStory userStory)
        {
            _userStoryDao.Update(userStory);
        }

        public void DeleteUserStory(UserStory userStory)
        {
            _userStoryDao.Delete(userStory);
        }

        public void RelateUserStories(UserStory mainStory, UserStory relatedStory, string relationship)
        {
            var relate = new RelatedUserStory()
            {
                MainStoryId = mainStory.Id,
                RelatedId = relatedStory.Id,
                Relationship = new StoryRelationship() { Description = relationship}
            };
            mainStory.RelatedStories.Add(relate);
            relatedStory.StoriesRelatedTo.Add(relate);
            _relatedUserStoryDao.Add(relate);
        }
        public void SetEditableTo(UserStory userStory, bool editable)
        {
            _userStoryDao.SetEditableTo(userStory, editable);
        }
    }
}