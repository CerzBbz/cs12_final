﻿using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ScrumManagementServer.Services
{
    public class TaskService
    {
        private readonly TaskDao _taskDao;
        private readonly UserStoryDao _userStoryDao;

        public TaskService(TaskDao dao = null, UserStoryDao userStoryDao = null)
        {
            _taskDao = dao ?? new TaskDao(new ScrumContextFactory());
            _userStoryDao = userStoryDao ?? new UserStoryDao(new ScrumContextFactory());
        }

        public void AddOrUpdateTask(Task task)
        {
            //Test code for add
            if(task.Id == 0 && task.AssociatedUserStoryId == 0)
            {
                if (!_userStoryDao.GetAllUserStories().Any())
                    _userStoryDao.Add(new UserStory
                    {
                        Title = "Test",
                        Priority = 4,
                        Summary = "TestSummary",
                        ImpactedArea = "UI",
                        StoryPointEstimate = 10
                    });

                var testUserStory = _userStoryDao.GetAllUserStories().First();
                testUserStory.AssociatedTasks.Add(task);
                _userStoryDao.Update(testUserStory);
            }
            else
            {
                var userStory = _userStoryDao.GetAllUserStories().FirstOrDefault(us => us.Id == task.AssociatedUserStoryId);
                _userStoryDao.Update(userStory);
            }
        }

        public void DeleteTask(Task task)
        {
            _taskDao.Delete(task);
        }

        public List<Tuple<DateTime, int>> GetDailyRevisionsForTask(Task task)
        {
            var revisions = _taskDao.GetAuditHistoryForTask(task);
            return (from revision in revisions
                    group revision by revision.ModifiedAt.Date into groupedRevisions
                    select Tuple.Create(groupedRevisions.Key, groupedRevisions.Select(t => t.RemainingHours).FirstOrDefault()))
                    .ToList();
        }
    }
}