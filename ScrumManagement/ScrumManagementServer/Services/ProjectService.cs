﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Services
{
    public class ProjectService
    {
        private readonly ProjectDao _projectDao;

        public ProjectService(ProjectDao dao = null)
        {
            _projectDao = dao ?? new ProjectDao(new ScrumContextFactory());
        }

        public void CreateProject(Project project)
        {
            _projectDao.Add(project);
        }

        public void UpdateProject(Project project)
        {
            _projectDao.Update(project);
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _projectDao.GetAllProjects();
        }

        public IEnumerable<UserStory> GetAllUserStoriesForProject(Project project)
        {
            return project.Stories.ToList();
        }

        public void AddSprintToProject(Project project, Sprint sprint)
        {
            project.Sprints.Add(sprint);
            _projectDao.Update(project);
        }

        public void AddUserStoryToProject(Project project, UserStory story)
        {
            //add UserStory to Project's list of UserStories
            project.Stories.Add(story);
            _projectDao.Update(project);
        }

        public void RemoveUserStoryFromProject(Project project, UserStory story)
        {
            project.Stories.Remove(story);
            _projectDao.Update(project);
        }

        public Project GetProjectFromId(int id)
        {
            var projects = _projectDao.GetAllProjects();
            return projects.FirstOrDefault(project => project.Id == id);
        }

        public List<UserStory> GetAllUserStoriesForProjectId(int id)
        {
            var project = GetProjectFromId(id);
            return project.Stories.ToList();
        }

        public List<Sprint> GetSprintsFromProject(Project project)
        {
            return _projectDao.GetSprintsFromIds(project.Sprints.Select(s => s.Id).ToList()).ToList();
        }
           
        public IEnumerable<Project> GetAllLazy()
            {
            return _projectDao.GetAllLazy();
        }

        public int UpdateUserStory(Project project, UserStory userStory)
        {
            if (!userStory.Editable)
            {
                return 0; // 0 means the story could not be edited
            }
            else
            {
                UpdateProject(project);
                return 1; //1 means it could, and was
            }
        }
    }
}