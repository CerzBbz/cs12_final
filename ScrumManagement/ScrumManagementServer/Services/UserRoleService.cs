﻿using ScrumManagementServer.Domain;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer.Services
{
    public class UserRoleService
    {
        private readonly BaseDao<UserRole> _userRoleDao;

        public UserRoleService()
        {
            _userRoleDao = new BaseDao<UserRole>(new ScrumContextFactory());
        }

        public UserRole GetUserRole(string description)
        {
            var role = _userRoleDao.GetSingle(ur => ur.RoleDescription.Equals(description));
            return role ?? new UserRole { RoleDescription = description };
        }
    }
}