﻿using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;
using ScrumManagementServer.Extensions;

namespace ScrumManagementServer.Services
{
    public class UserService
    {
        private readonly UserDao _userDao;
        private readonly UserRoleService _userRoleService;

        public UserService(UserDao dao = null)
        {
            _userDao = dao ?? new UserDao(new ScrumContextFactory());
            _userRoleService = new UserRoleService();
        }
        public void UpdateUserDetails(User user)
        {
            _userDao.Update(user);
        }


        public bool IsUsernameCurrentlyInUse(string username)
        {
            return _userDao.GetAllLazy().Any(u => u.Username.Equals(username));
        }

        public bool DoesUsernameExist(string userName)
        {
            return _userDao.GetAllLazy().Any(u => u.Username.Equals((userName)));
        }

        public bool IsExistingUser(User user)
        {
            return _userDao.GetAllLazy().Any(u => u.Username.Equals(user.Username) && u.Password.Equals(user.Password));
        }

        public void RegisterNewUser(User user)
        {
            _userDao.Add(user);
        }

        public void AddUserRole(User user, string role)
        {
            user.UserRoles.Add(_userRoleService.GetUserRole(role));
            if (user.Id != 0)
            {
                //When adding a role we only want to update if the user already exists 
                //in order to prevent duplicate users at registration
                //I.e post registration a role was added
                _userDao.Update(user);
            }                
        }

        public void RemoveUserRole(User user, string role)
        {
            user.UserRoles.Remove(_userRoleService.GetUserRole(role));
            //A role can only be remove post registration so it can be assumed the user exists to update
            _userDao.Update(user);
        }

        //method to return salt, which allows password input to be authenticated correctly
        public string FindSalt(User user)
        {
            var matchingUser = _userDao.GetAllLazy().FirstOrDefault(u => u.Username.Equals((user.Username)));
            return matchingUser != null ? matchingUser.Salt : "";
        }

        public int GetUserId(string username)
        {
            return _userDao.GetIdFromName(username);
        }

        /*
        GetUsernamesFromSearch returns usernames depending on if certain search paramaters are met
        email - string of email being searched
        skill - string of skills being searched
        po - boolean to see if searching by project owner
        dev - boolean to see if searching by developer
        sm - boolean to see if searching by scrum master
        */
        public List<string> GetUsernamesFromSearch(string email, string skill, bool po, bool dev, bool sm)
        {
            //TODO: Make str.toLower strings being compared so capital letters don't mess things up.
            //TODO: Change the skill searching to split skill and user.Skills up into string arrays split by commas, this way we can search for multiple skills.
            var idList = new List<string>();
            foreach (var user in _userDao.GetAllUsers())
            {
                bool isEmail = false, isSkill = false, add = false;
                if (email != null)
                {
                    isEmail = true;
                }
                if (skill != null)
                {
                    isSkill = true;
                }
                if (isSkill && isEmail)
                {
                    if (user.Username.ToLower().Contains(email.ToLower()))
                    {
                        if (user.Skills.IsNotNull() && user.Skills.ToLower().Contains(skill.ToLower()))
                        {
                            add = CheckRoles(user, po, dev, sm);
                        }
                    }
                }
                else if(isSkill)
                {
                    if (user.Skills.IsNotNull() && user.Skills.Contains(skill))
                    {
                        add = CheckRoles(user, po, dev, sm);
                    }
                }
                else if(isEmail)
                {
                    if (user.Username.Contains(email))
                    {
                        add = CheckRoles(user, po, dev, sm);
                    }
                }
                else
                {
                    add = CheckRoles(user, po, dev, sm);
                }
                if (add)
                {
                    idList.Add(user.Username);
                }
            }
            return idList;
        }

        /*
        CheckRoles method used to prevent repetition within the GetUsernamesFromSearch method
        Checks to see what roles are being met and then returns true if the specific user has this role.
        */
        private static bool CheckRoles(User user, bool po, bool dev, bool sm)
        {
            if (po && user.IsProductOwner())
            {
                return true;
            }
            if (dev && user.IsDeveloper())
            {
                return true;
            }
            return sm && user.IsScrumMaster();
        }

        public List<string> GetSkillsFromUsernames(List<string> usernames)
        {
            var skills = new List<string>();
            foreach (var name in usernames)
            {
                skills.AddRange(from user in _userDao.GetAllUsers() where name == user.Username select user.Skills);
            }
            return skills;
        }  

        public IEnumerable<User> GetAllUsers()
        {
            return _userDao.GetAllUsers();
        }

        public IEnumerable<User> GetAllLazy()
        {
            return _userDao.GetAllLazy();
        }

        public IEnumerable<User> GetUsersWithRole(string roleDescription)
        {
            return _userDao.GetAllUsers().Where(u => u.UserRoles.Any(ur => ur.RoleDescription.Equals(roleDescription)));
        }
    }
}