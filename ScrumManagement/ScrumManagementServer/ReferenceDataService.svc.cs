﻿using System.Collections.Generic;
using System.Linq;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Entity;

namespace ScrumManagementServer
{
    public class ReferenceDataService : IReferenceDataService
    {
        private readonly UserRoleDao _userRoleDao;

        public ReferenceDataService()
        {
            _userRoleDao = new UserRoleDao(new ScrumContextFactory());
        }

        public List<UserRole> GetAvailableUserRoles()
        {
            return _userRoleDao.GetAllUserRoles().ToList();
        }
    }
}
