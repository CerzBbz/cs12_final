﻿namespace ScrumManagementServer.Entity
{
    public interface IScrumContextFactory
    {
        ScrumContext GetContext();
    }
}
