﻿using System.Collections.Generic;
using System.Data.Entity;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer.Entity
{
    public class ScrumInitialiser : CreateDatabaseIfNotExists<ScrumContext>
    {
        protected override void Seed(ScrumContext context)
        {
            var userRoles = new List<UserRole>()
            {
                new UserRole {Id = 1, RoleDescription = "Product Owner"},
                new UserRole {Id = 2, RoleDescription = "Scrum Master"},
                new UserRole {Id = 3, RoleDescription = "Developer"}
            };

            var storyRelationships = new List<StoryRelationship>()
            {
                new StoryRelationship {Id = 1, Description = "Related"},
                new StoryRelationship {Id = 2, Description = "Depends on"},
                new StoryRelationship {Id = 3, Description = "Duplicate"}
            };

            userRoles.ForEach(ur => context.UserRoles.Add(ur));
            storyRelationships.ForEach(sr => context.StoryRelationships.Add(sr));
            base.Seed(context);
        }
    }
}