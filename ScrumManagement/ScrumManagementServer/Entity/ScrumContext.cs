﻿using System.Data.Entity;
using ScrumManagementServer.Domain;
using System.Linq;
using System;
using Omu.ValueInjecter;
using ScrumManagementServer.Injections;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;

namespace ScrumManagementServer.Entity
{
    public class ScrumContext : DbContext
    {
        //Tell entity which connection string to use
        public ScrumContext() : base("name=Dbconnection")
        {
            Database.SetInitializer(new ScrumInitialiser());
        }

        public ScrumContext(string empty)
        {
            //To be used in unit tests, prevent trying to access the production database   
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<UserStory> UserStories { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Task_AUD> Task_AUD { get; set; }
        public virtual DbSet<RelatedUserStory> RelatedUserStories { get; set; }
        public virtual DbSet<StoryRelationship> StoryRelationships { get; set; }
        public virtual DbSet<Sprint> Sprints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<BaseEntity>().HasKey(e => e.Id);

            modelBuilder.Entity<User>()
                .HasMany(u => u.UserRoles)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("User_UserRole");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("UserRoleId");
                });

            modelBuilder.Entity<Project>()
                .HasOptional(p => p.Owner)
                .WithMany()
                .HasForeignKey(u => u.OwnerId)
                .WillCascadeOnDelete(false); //if project is deleted, keep the user

            modelBuilder.Entity<Project>()
                .HasRequired(p => p.Manager)
                .WithMany()
                .HasForeignKey(u => u.ManagerId)
                .WillCascadeOnDelete(false); //if project is deleted, keep the user

            modelBuilder.Entity<Project>()
                .HasMany(p => p.Stories)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("ProductBacklog");
                    m.MapLeftKey("ProjectId");
                    m.MapRightKey("UserStoryId");
                });

            modelBuilder.Entity<Project>()
               .HasMany(s => s.Sprints)
               .WithMany()
               .Map(m =>
               {
                   m.ToTable("ProjectSprints");
                   m.MapLeftKey("ProjectId");
                   m.MapRightKey("SprintId");
               });

            modelBuilder.Entity<UserStory>()
                .HasMany(us => us.RelatedStories)
                .WithRequired(rs => rs.MainStory)
                .HasForeignKey(rs => rs.MainStoryId);

            modelBuilder.Entity<UserStory>()
                .HasMany(us => us.StoriesRelatedTo)
                .WithRequired(srt => srt.RelatedStory)
                .HasForeignKey(rs => rs.RelatedId)
                .WillCascadeOnDelete(false);

            //Ensure a task always has a title, for testing and query purposes
            modelBuilder.Entity<Task>()
                .Property(t => t.Title).IsRequired();

            modelBuilder.Entity<UserStory>()
                .HasMany(us => us.AssociatedTasks)
                .WithRequired(t => t.AssociatedUserStory)
                .HasForeignKey(t => t.AssociatedUserStoryId)
                .WillCascadeOnDelete(true); //Delete all tasks when user story is deleted

            modelBuilder.Entity<Task>()
                .HasOptional(t => t.AssignedTo)
                .WithMany(u => u.AssignedTasks)
                .HasForeignKey(t => t.AssignedToId)
                .WillCascadeOnDelete(false); //if user is deleted, the task should be available to re-assign

            modelBuilder.Entity<Sprint>()
               .HasMany(s => s.Developers)
               .WithMany()
               .Map(m =>
               {
                   m.ToTable("Sprint_Members");
                   m.MapLeftKey("SprintId");
                   m.MapRightKey("UserId");
               });

            modelBuilder.Entity<Sprint>()
             .HasMany(s => s.Stories)
             .WithMany()
             .Map(m =>
             {
                 m.ToTable("SprintBacklog");
                 m.MapLeftKey("SprintId");
                 m.MapRightKey("UserStoryId");
             });
        }
        public override int SaveChanges()
        {
            var auditedEntityStates = new List<EntityState> { EntityState.Added, EntityState.Modified, EntityState.Deleted };
            var allAuditedEntities = ChangeTracker.Entries()
                                        .Where(e => e.Entity.GetType().GetCustomAttributes(typeof(Audited), true).Length > 0)
                                        .Where(e => auditedEntityStates.Contains(e.State))
                                        .Select(e => e);

            //Only modify the save behaviour if we have something to audit
            if (!allAuditedEntities.Any())
                return base.SaveChanges();

            //Ensure all database modifications within this context SaveChanges show the same time, indicating a batch
            var batchModifiedAt = DateTime.Now;

            //Save to allow new entities to obtain and Id which can be used in auditing, but don't wipe tracked entities
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            objectContext.SaveChanges(SaveOptions.None);

            foreach (var entity in allAuditedEntities)
            {
                var set = GetAssociatedAuditTable(entity.Entity.GetType());
                var auditEntity = GetAuditEntity(entity.Entity);
                if (auditEntity != null)
                {
                    SetAuditProperties(auditEntity, entity.State, batchModifiedAt);
                    Entry(entity.Entity).State = EntityState.Unchanged;
                    set.Add(auditEntity);
                }
            }

            DetachAllNonAuditedEntities();
            return base.SaveChanges();
        }

        private void DetachAllNonAuditedEntities()
        {
            foreach (var entity in ChangeTracker.Entries().Where(e => !e.Entity.GetType().Name.EndsWith("_AUD") && (e.State == EntityState.Added || e.State == EntityState.Modified || e.State == EntityState.Deleted)))
            {
                Entry(entity.Entity).State = EntityState.Detached;
            }
        }

        private Type GetAuditTableType(Type mainEntityType)
        {
            var assembly = typeof(BaseEntity).Assembly;
            var targetTypeName = mainEntityType.Name + "_AUD";
            var auditTableType = assembly.GetType("ScrumManagementServer.Domain." + targetTypeName);
            return auditTableType;
        }

        private DbSet GetAssociatedAuditTable(Type mainEntityType)
        {            
            return Set(GetAuditTableType(mainEntityType));
        }

        private IAuditEntity GetAuditEntity(object sourceEntity)
        {
            var auditEntity = Activator.CreateInstance(GetAuditTableType(sourceEntity.GetType()));
            auditEntity.InjectFrom<AuditInjection>(sourceEntity);
            return auditEntity as IAuditEntity;
        }

        private void SetAuditProperties(IAuditEntity auditEntity, EntityState currentState, DateTime batchModifiedAt)
        {
            switch (currentState)
            {
                case EntityState.Added:
                    auditEntity.AuditType = AuditType.Created;
                    break;
                case EntityState.Deleted:
                    auditEntity.AuditType = AuditType.Deleted;
                    break;
                default:
                    auditEntity.AuditType = AuditType.Updated;
                    break;
            }
            auditEntity.ModifiedBy = "SYSTEM";
            auditEntity.ModifiedAt = batchModifiedAt;
        }
    }
}