﻿namespace ScrumManagementServer.Entity
{
    public enum AuditType
    {
        Created,
        Updated,
        Deleted
    }
}