﻿
using System;

namespace ScrumManagementServer.Entity
{
    interface IAuditEntity
    {
        AuditType AuditType { get; set; }
        string ModifiedBy { get; set; }
        DateTime ModifiedAt { get; set; }
    }
}