﻿namespace ScrumManagementServer.Entity
{
    public class ScrumContextFactory : IScrumContextFactory
    {
        public ScrumContext GetContext()
        {
            return new ScrumContext();
        }
    }
}