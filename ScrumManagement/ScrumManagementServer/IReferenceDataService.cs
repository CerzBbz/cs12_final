﻿using System.Collections.Generic;
using System.ServiceModel;
using ScrumManagementServer.Domain;

namespace ScrumManagementServer
{
    [ServiceContract]
    public interface IReferenceDataService
    {
        [OperationContract]
        List<UserRole> GetAvailableUserRoles();
    }
}
