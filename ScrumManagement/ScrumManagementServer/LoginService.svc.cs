﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Services;

namespace ScrumManagementServer
{
    public class LoginService : ILoginService
    {
        private readonly UserService _userService;

        public LoginService()
        {
            _userService = new UserService();
        }

        public LoginService(UserService userService)
        {
            _userService = userService ?? new UserService();
        }

        public LoginReturnCode AuthenticateUserDetails(string userName, string password, string salt)
            {
            var authenticatingUser = new User
            {
                Username = userName,
                Password = password,
                Salt =  salt
            };

            return _userService.IsExistingUser(authenticatingUser) ? LoginReturnCode.SuccessfulLogin : LoginReturnCode.InvalidLoginCredentials;
        }

        public LoginReturnCode RegisterNewUser(string userName, string password, string salt, string skills, List<UserRole> roles = null)
        {
            if (!IsUserNameAValidEmailAddress(userName))
                return LoginReturnCode.InvalidEmailAddress;
            if (!DoesPasswordMeetRequiredComplexity(password))
                return LoginReturnCode.InvalidPasswordComplexity;

            if (_userService.IsUsernameCurrentlyInUse(userName))
                return LoginReturnCode.UsernameAlreadyExists;

            var registeringUser = new User
            {
                Username = userName,
                Password = password,
                Salt = salt,
                Skills = skills,
                UserRoles = roles ?? new List<UserRole>()
            };

            _userService.RegisterNewUser(registeringUser);
            return LoginReturnCode.SuccessfulRegistration;
        }

        public bool IsUserNameValid(string userName)
        {
            return _userService.DoesUsernameExist(userName);
        }

        public string GetSalt(User user)
        {
            
            return _userService.FindSalt(user);
        }

        private bool IsUserNameAValidEmailAddress(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                    return false;
                var address = new MailAddress(username);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool DoesPasswordMeetRequiredComplexity(string password)
        {
            return !string.IsNullOrEmpty(password) && password.Length >= 8;
        }        
    }
}
