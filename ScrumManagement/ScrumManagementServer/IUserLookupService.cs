﻿using ScrumManagementServer.DataContracts;
using ScrumManagementServer.Enum;
using System.Collections.Generic;
using System.ServiceModel;

namespace ScrumManagementServer
{
	[ServiceContract]
	public interface IUserLookupService
	{
        [OperationContract]
        List<string> GetUsernamesFromSearch(string email, string skill, bool po, bool dev, bool sm);


    }
}
