﻿using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Domain;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ScrumManagementServer
{
    [ServiceContract]
    public interface ISprintManagementService
    {
        [OperationContract]
        void CreateSprint(ProjectDto project, string title, int scrumMasterID, DateTime startDate, DateTime endDate, List<UserDto> developers);

        [OperationContract]
        List<UserStoryDto> GetUserStoriesFromSprintId(int id);

        [OperationContract]
        List<UserStoryDto> GetUserStoriesFromSprint(SprintDto sprint);

        [OperationContract]
        int AddUserStoryToSprint(SprintDto sprint, UserStoryDto story);

        [OperationContract]
        int RemoveUserStoryFromSprint(SprintDto sprint, UserStoryDto story);

		[OperationContract]
        UserDto GetScrumMasterFromSprint(int sprintId);

        [OperationContract]
        void UpdateUserStory(UserStoryDto userStory);

        [OperationContract]
        int GetTotalEstimatedHoursOnTasksForSprint(SprintDto sprint);

        [OperationContract]
        List<List<Tuple<DateTime, int>>> GetRemainingHoursOnTasksFromSprintDaily(SprintDto sprint);

        [OperationContract]
        List<SprintDto> GetAllSprints();
    }
}
