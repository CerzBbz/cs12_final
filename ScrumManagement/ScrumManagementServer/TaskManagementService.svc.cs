﻿using System;
using System.Collections.Generic;
using ScrumManagementServer.DataContracts.DTO_classes;
using ScrumManagementServer.Services;
using ScrumManagementServer.Entity;
using ScrumManagementServer.Dao;
using ScrumManagementServer.Domain;
using ScrumManagementServer.Injections;

namespace ScrumManagementServer
{
    public class TaskManagementService : ITaskManagementService
    {
        private readonly ScrumContextFactory _contextFactory = new ScrumContextFactory();
        private readonly TaskService _taskService;

        public TaskManagementService()
        {
            _taskService = new TaskService(new TaskDao(_contextFactory));
        }

        public TaskManagementService(TaskService taskService)
        {
            _taskService = taskService ?? new TaskService();
        }

        public void CreateOrUpdateTask(TaskDto task)
        {
            _taskService.AddOrUpdateTask(task.ConvertToEntity() as Task);
        }

        public void DeleteTask(TaskDto taskToDelete)
        {
            _taskService.DeleteTask(taskToDelete.ConvertToEntity() as Task);
        }

        public List<Tuple<DateTime, int>> GetDailyHoursRevisionsForTask(TaskDto task)
        {
            throw new NotImplementedException();
        }
    }
}